var path = require('path'); // 为了得到项目根路径
var webpack = require('webpack'); // webpack核心
var ExtractTextPlugin = require('extract-text-webpack-plugin'); // 为了单独打包css
// var HtmlWebpackPlugin = require('html-webpack-plugin'); //根据模板生成最终html文件

module.exports = {
    entry: path.join(__dirname, '/src/index.js'),
    output: {
        publicPath: "/hdatamonitor/",
        path: path.join(__dirname, '/build'),
        filename: "bundle.js"
    },
    module: {
        rules: [
            {test: /\.(png|jpg)$/, use: 'url-loader?limit=100&name=images/[name].[ext]'},
            {
                test: /\.(js|jsx)?$/, exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader'
                }],
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            }
        ]
    },
    plugins: [
        //热模块替换插件(Uncaught Error: [HMR] Hot Module Replacement is disabled)
        new webpack.HotModuleReplacementPlugin(),
        //js文件压缩
        new webpack.optimize.UglifyJsPlugin(),
        //指定css文件名 打包成一个css
        new ExtractTextPlugin('style.css'),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify(process.env.NODE_ENV),
            },
        }),
    ],
    // ant需要
    resolve: {
        modules: ['node_modules', path.join(__dirname, './node_modules')],
        extensions: ['.jsx', '.js', '.json'], // webpack2 不再需要一个空的字符串
    },
    // 不需要打包的模块
    // externals: {
    //     "react": 'React',
    //     "react-dom": "ReactDOM"
    // }
};
console.log('........................')
console.log(process.env.NODE_ENV)
