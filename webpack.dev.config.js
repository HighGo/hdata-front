
var path = require('path');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    entry: ['webpack/hot/dev-server', path.join(__dirname, '/src/index.js')],
    output:{
        // publicPath: "/hdatamonitor",
        path: path.join(__dirname, '/build'),
        filename:"bundle.js"//打包后输出的文件
    },
    module:{
        rules:[
            {
                test: /\.(png|jpg)$/,
                use: 'url-loader?limit=100&name=images/[name].[ext]'
                // use: 'url-loader?name=images/[name].[ext]'
            },
            {
                test:/\.(js|jsx)?$/,
                exclude:/node_modules/,
                use: [{
                    loader: 'babel-loader'
                }],
            },{
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: "css-loader"
                })
            }
        ]
    },
    plugins: [
        //热模块替换插件(Uncaught Error: [HMR] Hot Module Replacement is disabled)
        new webpack.HotModuleReplacementPlugin(),
        //指定css文件名 打包成一个css
        new ExtractTextPlugin("style.css")
    ],
    // ant需要
    resolve: {
        modules: ['node_modules', path.join(__dirname, './node_modules')],
        extensions: ['.jsx', '.js', '.json'], // webpack2 不再需要一个空的字符串
    },
    // 不需要打包的模块
    // externals: {
    //     "react": 'React',
    //     "react-dom": "ReactDOM"
    // },
    //webpack-dev-server配置
    devServer: {
        contentBase: './build',//默认webpack-dev-server会为根文件夹提供本地服务器，如果想为另外一个目录下的文件提供本地服务器，应该在这里设置其所在目录（本例设置到"build"目录）
        historyApiFallback: true,//在开发单页应用时非常有用，它依赖于HTML5 history API，如果设置为true，所有的跳转将指向index.html
        inline: true,//设置为true，当源文件改变时会自动刷新页面
        port: 7000,//设置默认监听端口，如果省略，默认为"8080"
        disableHostCheck: true,//open hostname,else invalid host header
        host: '0.0.0.0'
    },
}
