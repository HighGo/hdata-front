import React from 'react';
import ReactDOM from 'react-dom';
import './css/index.css'


import Main from './components/Main';
import HdataRouter from './components/HdataRouter'
// import Security from './components/test/Security';

require('es6-promise').polyfill();

// import Login from './components/NormalLoginForm';

ReactDOM.render(
    <HdataRouter/>
    , document.getElementById('root'));