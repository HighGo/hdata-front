import React from 'react';
import reqwest from 'reqwest';

import {Table, Modal} from 'antd';

const confirm = Modal.confirm;

import Properties from '../constants/Properties';
import * as Constants from '../constants/Constants';


export default class WatchTbale extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            pagination: {current: 1, pageSize: 10, total: 0},
            loading: false,
        };
    };

    componentDidMount = () => {//did mount
        this.init(this.props.id)
    };

    componentWillReceiveProps(nextProps) {//will receive
        console.log('watchTable...componentWillReceiveProps.....' + nextProps.id)
        clearInterval(this.timerID);
        //reset page curr
        const pager = {...this.state.pagination};
        pager.current = 1;
        this.setState({pagination: pager});
        //reset
        this.init(nextProps.id);
    };

    componentWillUnmount() {//unmount
        clearInterval(this.timerID);
    };

    handleTableChange = (pagination, filters, sorter) => {
        console.log('watchTable.....handleTableChange......')

        const pager = {...this.state.pagination};
        pager.current = pagination.current;
        this.setState({pagination: pager,});
        this.fetch({
            deviceId: this.props.id,
            pageSize: pagination.pageSize,
            current: pagination.current,
            // sortField: sorter.field,
            // sortOrder: sorter.order,
            //...filters,
        });
    };
    init = (id) => {
        this.fetch({
            method: 'get'
            , deviceId: id
            , current: 1
        });

        this.timerID = setInterval(
            () => {
                const page = this.state.pagination;
                this.fetch({
                    method: 'get'
                    , deviceId: id
                    , current: page.current
                })
            },
            Properties.getMonitorLoginfoURL_TIME
        );
    };
    fetch = (params = {}) => {
        console.log('watchTable.....fetch......')
        // console.log('params:', params);
        this.setState({loading: true});
        const that = this;

        console.log(params)
        reqwest({
            url: Properties.getMonitorLoginfoURL
            , method: params.method
            , type: 'json'
            , data: {
                ...params,
            }
            , contentType: 'application/json'
            , error: function (err) {
                console.log(err);
                that.setState({loading: false});
            }
            , success: function (resp) {
                console.log(resp)
                if (resp.success) {
                    // console.log(resp)
                    const pagination = {...that.state.pagination};
                    pagination.total = resp.data.totalCount;
                    pagination.current = params.current;

                    // console.log('reqwest....' +params.current)
                    that.setState({
                        loading: false,
                        data: resp.data.results,
                        pagination,
                    });
                    // console.log('reqwest....end')
                } else {
                    console.log(params.url)
                    console.log(resp);
                }
            }
        })
    };
    handleFilterTextInput = (text, time) => {//search
        this.fetch({
            search: text,
            time: time
        });
    };
    showConfirm = () => {
        const that = this;
        const url = this.props.url;
        const id = this.props.id;
        // console.log('showConfirm');
        // console.log('watchTable...showConfirm...' + id);
        confirm({
            title: Constants.DELETE_MONITORLOG_TITLE,
            // content: 'When clicked the OK button, this dialog will be closed after 1 second',
            onOk() {
                return new Promise((resolve, reject) => {
                    reqwest({
                        url: Properties.deleteMonitorLogURL
                        , method: 'get'
                        , type: 'json'
                        , data: {
                            deviceId: id
                        }
                        , contentType: 'application/json'
                        , error: function (err) {
                            console.log(err)
                        }
                        , success: function (resp) {
                            console.log(resp)
                            that.fetch({
                                method: 'get'
                                , deviceId: id
                                , current: 1
                            });
                        }
                    });
                    setTimeout(Math.random() > 0.5 ? resolve : reject, 1000);
                }).catch((e) => {
                        console.log('Oops errors!')
                        console.log(e)
                    }
                );
            },
            onCancel() {
            },
        });
    }

    render() {
        const columns = [
            {
                title: 'level',
                dataIndex: 'level',
                width: '10%',
                render: function (a, b, c) {
                    if (a === Constants.WARN) {
                        return Constants.WARN_MSG
                    } else if (a === Constants.ERROR) {
                        return Constants.ERROR_MSG;
                    }
                }
            }, {
                title: 'time',
                dataIndex: 'time',
                width: '20%',
            }, {
                title: 'name',
                dataIndex: 'name',
                width: '20%',
            }
            , {
                title: 'message',
                dataIndex: 'message'
            }];

        return (
            <div>
                {/*<SearchForm onFileInput={this.handleFilterTextInput}/>*/}
                <div className="searchForm">
                    <div className="log">日志</div>
                    <div className="clear-log">
                        <span onClick={this.showConfirm}>清除所有日志（{this.state.pagination.total}）</span>
                    </div>
                </div>
                <Table rowKey={record => record.key}
                       dataSource={this.state.data}
                       pagination={this.state.pagination}
                       loading={this.state.loading}
                       onChange={this.handleTableChange}
                       columns={columns}
                       showHeader={false}
                       bordered
                       center
                       scroll={{y: 160}}
                       size="small"/>
            </div>

        )
    }
}
