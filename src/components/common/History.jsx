import React from 'react';
import {Button} from 'antd';

import StatisticsLineDay from './StatisticsLineDay';
import StatisticsLineHour from './StatisticsLineHour';
import HistoryTable from './HistoryTable';

export default class History extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            view: 1,//1:day,2:hour
            device: {
                deviceId: '',
                name: '',
                time: '',
                graphUrl: '',
                tableUrl: '',
                currDay: false,
                type: this.props.type
            }
        };
    };

    componentWillReceiveProps(nextProps) {
        this.setState({view: 1});//init
    };

    handRecover = () => {
        console.log('handRecover...');
        this.setState({view: 1})
    };
    handOnDBClickEcharts = (deviceId, name, time, graphUrl, tableUrl) => {
        //dbclick echarts event
        console.log('handOnDBClickEcharts...');
        this.setState({
            view: 2,
            device: {
                deviceId: deviceId,
                name: name,
                time: time,
                graphUrl: graphUrl,
                tableUrl: tableUrl,
                type: this.props.type
            }
        })
    };

    render() {
        console.log('History...........')
        const deviceId = this.props.deviceId;
        return (
            <div className="history-index" style={{width: '96%', margin: '0px auto'}}>
                {this.state.view === 1
                    ? <ItemList data={this.props.list}
                                deviceId={deviceId}
                                onClickEcharts={this.handOnDBClickEcharts}
                    />
                    : <Details id="Details"
                               data={this.state.device}
                               onClickRecover={this.handRecover}
                    />}
            </div>
        )
    }
};

import reqwest from 'reqwest';

class ItemList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemList: []
        };
    }

    componentDidMount = () => {
        let that = this;
        const list = this.props.data.list;
        reqwest({
            url: this.props.data.url
            , method: 'get'
            , type: 'json'
            , contentType: 'application/json'
            , success(resp) {
                // console.log(resp);
                for (let i = 0; i < list.length; i++) {
                    list[i].dataDetail = resp.data[list[i].index]
                }
                // console.log(list);
                that.setState({itemList: list});
            }, error(resp) {
                console.log(resp);
            }
        })
    };

    render() {
        let that = this;
        console.log('ItemList...............');
        // console.log(this.state.itemList);
        return (
            <div>
                {
                    this.state.itemList.map(function (item, index) {
                        // console.log(item)
                        return (
                            <div key={index}>
                                <h3>{item.name}</h3>
                                <div className="history-item">
                                    <StatisticsLineDay id={item.id}
                                                       deviceId={that.props.deviceId}
                                                       data={item}
                                                       minHeight="180px"
                                                       onClickEcharts={that.props.onClickEcharts}//onclick return hour view
                                                       substring={8}//show echarts x

                                    />
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}


const Details = (props) => {
    return (
        <div>
            <h3>{props.data.name}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{props.data.time}</h3>
            <div className="history-item">
                <StatisticsLineHour
                    id={props.id}
                    graphUrl={props.data.graphUrl}
                    minHeight="180px"
                    data={props.data}
                    substring={11}//show echarts x
                />
                <HistoryTable data={props.data}/>
                <Button icon="rollback" style={{margin: '20px 0px 20px 0px'}} onClick={props.onClickRecover}>返回</Button>
            </div>
        </div>
    )
};