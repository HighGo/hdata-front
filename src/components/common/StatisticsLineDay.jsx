import React from 'react';

import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markLine';
import 'echarts/lib/component/dataZoom';

import EchartsUtils from '../common/EchartsUtils';

var colorList = ['#70D9C0', '#FFCE57', '#EC8CC2'];


export default class StatisticsLineDay extends React.Component {
    componentDidMount = () => {
        const that = this;

        // console.log(that.props.data)
        let myChart = echarts.init(document.getElementById(this.props.id));
        let option = this.setPieOption(this.props.data.dataDetail);
        myChart.setOption(option);

        myChart.on('dblclick', function (params) {
            //get onclick time
            that.props.onClickEcharts(
                that.props.deviceId,
                that.props.data.name,
                params.name,
                that.props.data.graphUrl,
                that.props.data.tableUrl,
            );
        });
        window.addEventListener("resize", this.handler, false);
    };

    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID, echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        EchartsUtils.handler(echarts, this.props.id);
    };
    setPieOption = (data) => {
        // console.log(data);

        let legent = [];
        let series = [];
        let xdata = data.xdata;
        let dataZoom = [];
        let substring = this.props.substring;
        data.loadLineList.forEach(function (value, index, array) {
            series.push({
                type: 'line',
                name: value.name,
                data: value.text,
            });
        });

        data.legend.forEach(function (value, index, array) {
            legent.push({
                name: value,
                icon: 'rect'
            });
        });
        dataZoom.push({
            // start: 0,
            startValue: data.startTime,
            end: 100,
            handleIcon: 'M0,0 v9.7h5 v-9.7h-5 Z',
            handleSize: '70%',
            handleStyle: {
                color: '#fff',
                shadowBlur: 3,
                shadowColor: 'rgba(0, 0, 0, 0.6)',
                shadowOffsetX: 2,
                shadowOffsetY: 2
            },
        });


        return {
            legend: {
                show: true,
                left: 'right',
                top: 'middle',
                orient: 'vertical',
                data: legent,
                selectedMode: false,
                itemWidth: 20,
                itemHeight: 8,
            },
            tooltip: {
                position: function (pos, params, dom, rect, size) {
                    // 鼠标在左侧时 tooltip 显示到右侧，鼠标在右侧时 tooltip 显示到左侧。
                    var obj = {top: -30};
                    obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
                    return obj;
                },
                trigger: 'axis',
                // formatter: '{a0}:{b0}: {c0}<br />{a1}::{b1}: {c1}',
                axisPointer: {
                    type: 'line',
                    label: {
                        show: false,
                        backgroundColor: '#6a7985',
                        textStyle: {
                            fontSize: 20
                        },
                        // padding:[10,7,5,7]
                    },
                    // crossStyle:{
                    //     opacity:0
                    // }
                },
                textStyle: {
                    fontSize: 12,
                    align: 'left'
                }
                // confine: true
            },
            dataZoom: dataZoom,
            grid: {
                show: true,
                top: '20',
                left: '20',
                width: '88%',
                height: '60%',
                // borderColor:'#ccc',
                borderWidth: 0,
                containLabel: true
            },
            xAxis: {
                show: true,
                type: 'category',
                data: xdata,
                splitNumber: 30,
                // boundaryGap: '10%'  //'10%|false'
                axisLabel: {
                    rotate: 0,
                    formatter: function (val) {
                        // console.log(val)
                        return val.substring(substring, substring + 2);
                    }
                }
            },
            yAxis: {
                show: true,
                axisLabel: {
                    show: true
                },
                splitNumber: 2,
                // max: 100
            },
            series: series,
            color: colorList
        }
    };

    render() {
        console.log('StatisticsLineDay........');
        return (
            <div id={this.props.id} style={{
                width: "100%",
                minHeight: this.props.minHeight,
                margin: '0px auto',
                backgroundColor: '#fff',
                border: '1px solid #e9e9e9'
            }}/>
        );
    }
}