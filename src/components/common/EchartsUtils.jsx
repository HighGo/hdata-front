/**
 * 图标工具类
 *
 */

export default class EchartsUtils {
    static handler = (echarts,id) => {
        let myChart = echarts.getInstanceByDom(document.getElementById(id));
        if (myChart !== undefined) {
            myChart.setOption(myChart.getOption);
            myChart.resize();
        }
    };

    static Unmount = (timerID, echarts, id) => {
        if (timerID !== null && timerID !== undefined) {
            clearInterval(timerID);
        }
        let myChart = echarts.getInstanceByDom(document.getElementById(id));
        if (myChart !== undefined) {
            myChart.dispose();
            window.removeEventListener("resize", EchartsUtils.handler(echarts, id), false);
        }
    }
}