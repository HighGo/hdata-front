import React from 'react';
import moment from 'moment';
import {Form, Input, Button, DatePicker} from 'antd';

import searchImg from '../../img/icon-search.png'
const FormItem = Form.Item;

var dateFormat = 'YYYY-MM-DD';

// not used !!!
class SearchForm extends React.Component {

    handleSearch = (e) => {
        console.log(e)
        e.preventDefault();
        const validate = this.props.form.validateFields((err, fieldsValue) => {
            if (err) {
                return;
            }else {
                let text = this.props.form.getFieldValue('searchInput');
                let time = this.props.form.getFieldValue('datePicker');
                this.props.onFileInput(text,time.format(dateFormat))
            }
        });
    };
    handleReset = (e) => {
        this.props.form.resetFields();
    };
    handleReset1 = (e,ev) => {
        this.props.form.resetFields();
    };
    render() {
        const {getFieldDecorator} = this.props.form;

        const searchInputConfig = {
            initialValue:''
        };
        const datePickerConfig = {
            // rules: [{ type: 'object', required: true, message: ' ' }],
            initialValue:moment(new Date(), dateFormat)
        };

        return (
            <Form layout="inline" className="ant-advanced-search-form searchForm"
                  onSubmit={this.handleSearch} style={{height:"35px"}}>

                <div className="ant-row ant-form-item datePicker">日志1</div>
                <FormItem style={{marginBottom: 0,lineHeight:'33px'}}>
                    {getFieldDecorator('datePicker',datePickerConfig)(
                        <DatePicker format={dateFormat}/>
                    )}
                </FormItem>
                <FormItem style={{marginBottom: 0,lineHeight:'33px'}} >
                    {getFieldDecorator('searchInput',searchInputConfig)(
                        <Input placeholder="请输入关键字搜索" />
                    )}
                </FormItem>
                <img alt="" src={searchImg} style={{marginTop:"4px"}} onClick={this.handleSearch}/>
                <div className="ant-row ant-form-item datePicker">清除所有日志</div>
            </Form>
        );
    }
}

export default SearchForm = Form.create()(SearchForm);
