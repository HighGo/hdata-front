import React from 'react';
import reqwest from 'reqwest';

import {Table} from 'antd';
import Properties from '../constants/Properties';

export default class HistoryTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            url: this.props.data.tableUrl,
            params: this.props.data,
            tableData: null,
            pagination: {pageSize: 10, total: 0},
            loading: false,
        };
        // console.log('constructor');
        // console.log(this.state.params)
    };

    componentDidMount = () => {//did mount
        this.init(this.state.url)
    };

    componentWillUnmount() {//unmount
        clearInterval(this.timerID);
    };

    handleTableChange = (pagination, filters, sorter) => {
        // console.log(pagination);

        const pager = {...this.state.pagination};
        pager.current = pagination.current;
        this.setState({
            pagination: pager,
        });
        const params = this.state.params;

        params.pageSize = pagination.pageSize;
        params.current = pagination.current;

        // console.log(params);
        this.fetch({
            url: this.state.url
            , method: 'get'
            , data: params
        });
    };
    init = (url) => {
        const params = this.state.params;
        this.fetch({
            url: url
            , method: 'get'
            , data: params
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url
                , method: 'get'
                , data: params
            }),
            Properties.getHistoryDayURL_TIME
        );


    };
    fetch = (params = {}) => {
        this.setState({loading: true});
        let that = this;

        // console.log(params)

        reqwest({
            url: params.url,
            method: params.method
            , data: params.data
            , type: 'json'
            , contentType: 'application/json', error: function (err) {
                console.log(err)
                that.setState({
                    loading: false
                });
            }
        }).then((resp) => {
            //resp
            console.log(resp);
            if (resp.success) {
                const pagination = {...that.state.pagination};
                pagination.total = resp.data.totalCount;
                this.setState({
                    tableData: resp.data,
                    pagination,
                    loading: false
                })
            } else {
                console.log(params.url);
                console.log(resp);
                that.setState({
                    loading: false
                });
            }
        });
    };

    render() {
        if (this.state.tableData === null) {
            return null;
        }

        const columns = [];
        columns.push({
            title: "时间",
            dataIndex: 'sampleTime',
            render: function (text, record, index) {
                return getMyDate(text);
            },
            width: '15%',
        });
        // console.log(this.state.tableData.columns)
        this.state.tableData.columns.forEach(function (item, index) {
            columns.push({
                title: item.title,
                dataIndex: item.column,
                width: '22%'
            })
        });

        return (
            <div style={{marginTop: '20px'}}>
                <Table rowKey={record => record.id}
                       columns={columns}
                       dataSource={this.state.tableData.loadLineList}
                       pagination={this.state.pagination}
                       size="small"
                       scroll={{x: true, y: 300}}
                       bordered
                       loading={this.state.loading}
                       onChange={this.handleTableChange}
                />
            </div>
        )
    }
}

function getMyDate(str) {
    var oDate = new Date(str),
        oYear = oDate.getFullYear(),
        oMonth = oDate.getMonth() + 1,
        oDay = oDate.getDate(),
        oHour = oDate.getHours(),
        oMin = oDate.getMinutes(),
        oSen = oDate.getSeconds(),
        oTime = oYear + '-' + getzf(oMonth) + '-' + getzf(oDay) + ' ' + getzf(oHour) + ':' + getzf(oMin) + ':' + getzf(oSen);
    return oTime;
};

function getzf(num) {
    if (parseInt(num) < 10) {
        num = '0' + num;
    }
    return num;
}