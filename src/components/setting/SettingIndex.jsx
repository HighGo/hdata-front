import React from 'react';
import parameter from '../../img/icon-parameter.png';
import threshold from '../../img/icon-threshold.png';

import Param from './Param';
import Threshold from './Threshold';

import '../../css/setting.css';

export default class SettingIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tab: 1
        }
    }
    handTabClick = (tab) => {
        this.setState({
            tab: tab
        })
    };
    render() {
        return (
            <div className="setting">
                <div className="header">
                    <span className="title">设置</span>
                    <div style={{marginLeft: '5%'}}
                         className={this.state.tab === 1 ? 'hdata-tabs-tab hdata-tabs-tab-active' : 'hdata-tabs-tab'}
                         onClick={this.handTabClick.bind(this, 1)}>
                        <img alt="" src={parameter} className=""/>参数设置
                    </div>
                    <div className={this.state.tab === 2 ? 'hdata-tabs-tab hdata-tabs-tab-active' : 'hdata-tabs-tab'}
                         onClick={this.handTabClick.bind(this, 2)}>
                        <span><img alt="" src={threshold}/>阈值设置</span>
                    </div>
                </div>

                {this.state.tab === 1 ? <Param /> : <Threshold/>}
            </div>
        )
    }
}