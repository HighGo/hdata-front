import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import confirm2 from '../../img/btn-confirm2.png';

export default class Param extends React.Component {
    state = {
        time: 60,
        resultMsg: ''
    };
    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getConfigureHoldTimeURL
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp){
                console.log(resp)
                if (resp.success) {
                    that.setState({time: resp.data});
                }
            }, error(resp){
                console.log(Properties.getConfigureHoldTimeURL);
                console.log(resp);
            }
        })
    };
    onClickEvent = () => {
        let that = this;
        reqwest({
            url: Properties.saveConfigureHoldTimeURL + this.state.time
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp){
                console.log(resp)
                if (resp.success) {
                    that.setState({resultMsg: '系统提示：保存参数重启服务后生效！'})
                }
            }, error(resp){
                console.log(Properties.saveConfigureHoldTimeURL + this.state.time);
                console.log(resp);
            }
        })
    };
    handleNumberChange = (e) => {
        const number = parseInt(e.target.value || 0, 10);
        if (isNaN(number)) {
            return;
        }
        this.setState({time: number});
    };

    render() {
        return (
            <div className="param">
                <div>
                    监控指标保留时间（天）
                    <input className="input"
                           value={this.state.time}
                           onChange={this.handleNumberChange}
                    />
                </div>
                <h3 style={{color: 'red'}}>{this.state.resultMsg}</h3>
                <img alt="" src={confirm2} className="button" onClick={this.onClickEvent}/>
            </div>
        )
    }
}