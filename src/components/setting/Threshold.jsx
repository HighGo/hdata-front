import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import * as Constants from '../constants/Constants';

import Demo1 from './Demo1';

export default class Threshold extends React.Component {
    state = {
        resultMsg: []
    };
    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getThresholdURL
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp){
                console.log(resp)
                if (resp.success) {
                    that.setState({resultMsg: resp.dataList});
                }
            }, error(resp){
                console.log(Properties.getThresholdURL);
                console.log(resp);
            }
        })
    };

    render() {
        // const list = [
        //     {id:1,monitorType:"CPU状态",warnValue:'90%',errorValue:'98%'},
        //     {id:2,monitorType:"会话数",warnValue:'90%',errorValue:'98%'},
        //     {id:3,monitorType:"平均负载",warnValue:'90%',errorValue:'98%'},
        //     {id:4,monitorType:"内存使用情况",warnValue:'90%',errorValue:'98%'},
        //     {id:5,monitorType:"挂载点使用率",warnValue:'90%',errorValue:'98%'},
        //     {id:6,monitorType:"inodes使用率",warnValue:'90%',errorValue:'98%'},
        //     {id:7,monitorType:"Load（%cores）",warnValue:'90%',errorValue:'98%'}
        // ];
        return (
            <div className="threshold">
                <table className="table" cellSpacing={0} cellPadding={0}>
                    <tbody>
                    <tr className="title">
                        <td>指标</td>
                        <td>告警</td>
                        <td>报错</td>
                    </tr>
                    {
                        this.state.resultMsg.map(function (item,index) {
                            let text = item.monitorType;
                            if (item.monitorType === Constants.THRESHOLD_CPU) {
                                text = Constants.THRESHOLD_CPU_TEXT;
                            } else if (item.monitorType === Constants.THRESHOLD_SESSION) {
                                text = Constants.THRESHOLD_SESSION_TEXT;
                            } else if (item.monitorType === Constants.THRESHOLD_LOAD) {
                                text = Constants.THRESHOLD_LOAD_TEXT;
                            } else if (item.monitorType === Constants.THRESHOLD_MEMORY) {
                                text = Constants.THRESHOLD_MEMORY_TEXT;
                            } else if (item.monitorType === Constants.THRESHOLD_MOUNT) {
                                text = Constants.THRESHOLD_MOUNT_TEXT;
                            } else if (item.monitorType === Constants.THRESHOLD_INODES) {
                                text = Constants.THRESHOLD_INODES_TEXT;
                            }
                            return (
                                <tr key={index}>
                                    <td width={230}>{text}</td>
                                    <td width={140}>{item.warnValue}%</td>
                                    <td width={140}>{item.errorValue}%</td>
                                </tr>
                            )
                        })
                    }
                    </tbody>
                </table>
                <Demo1/>
            </div>
        )
    }
}