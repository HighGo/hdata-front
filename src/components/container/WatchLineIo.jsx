import React from 'react';
import EchartsUtils from '../common/EchartsUtils';

import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';


export default class WatchLineIo extends React.Component {
    componentDidMount = () => {
        this.initPie();
    };

    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID,echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        EchartsUtils.handler(echarts, this.props.id);
    };
    dynamic = (nextProps) => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let option = myChart.getOption();

        let loadLineList = nextProps.watchLineData.loadLineList;
        let lastTime = option.xAxis[0].data[option.xAxis[0].data.length - 1];

        option.series.forEach(function (item, index) {
            let obj = loadLineList[index].text[loadLineList[index].text.length - 1];

            option.series[index].data.shift();//step option series
            if (lastTime === obj.time) {
                //webService data error
                obj.value = "NaN";
            }
            option.series[index].data.push(obj);
        });

        let xAxisValue = nextProps.watchLineData.xdata[nextProps.watchLineData.xdata.length - 1];
        option.xAxis[0].data.shift();//step option xAxis
        option.xAxis[0].data.push(xAxisValue);

        myChart.setOption(option);
    };

    componentWillReceiveProps(nextProps) {
        // console.log('WatchLineIo..........componentWillReceiveProps...........');
        // console.log(nextProps);
        this.dynamic(nextProps);
    };

    initPie = () => {
        let myChart = echarts.init(document.getElementById(this.props.id));
        let option = this.setPieOption(this.props.watchLineData);
        myChart.setOption(option);

        window.addEventListener("resize", this.handler, false);
    };
    setPieOption = (data) => {
        let that = this;
        let title = data.title;
        let series = [];
        let xdata = data.xdata;

        data.loadLineList.forEach(function (value, index, array) {
            let show = value.show;

            if (show) {
                series.push({
                    type: 'line',
                    name: value.name,
                    data: value.text,
                    areaStyle: {
                        normal: {
                            color: that.props.areaStyle[index]
                        }
                    },
                });
            } else {
                series.push({
                    type: 'line',
                    name: value.name,
                    data: value.text,
                    lineStyle: {
                        normal: {
                            width: 0
                        }
                    },
                    symbolSize: 0
                });
            }
        });

        return {
            title: {
                text: title,
                top: 'bottom',
                left: 'center',
                textStyle: {
                    fontSize: 13,
                    fontWeight: 'normal'
                }
            },
            tooltip: {
                position: function (pos, params, dom, rect, size) {
                    // 鼠标在左侧时 tooltip 显示到右侧，鼠标在右侧时 tooltip 显示到左侧。
                    var obj = {top: -30};
                    obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
                    return obj;
                },
                trigger: 'axis',
                // formatter: '{a0}:{b0}: {c0}<br />{a1}::{b1}: {c1}<br />{a2}::{b2}: {c2}',
                axisPointer: {
                    type: 'line',
                    label: {
                        show: false,
                        backgroundColor: '#6a7985',
                        textStyle: {
                            fontSize: 20
                        },
                        // padding:[10,7,5,7]
                    },
                    // crossStyle:{
                    //     opacity:0
                    // }
                },
                textStyle: {
                    fontSize: 12,
                    align: 'left'
                },
                // confine: true
            },
            grid: {
                show: true,
                top: '10',
                left: '0',
                width: '90%',
                height: '90%',
                // borderColor:'#ccc',
                borderWidth: 0,
                containLabel: true
            },
            xAxis: {
                show: false,
                type: 'category',
                data: xdata,
                // type: 'time',
                axisLine: {
                    lineStyle: {
                        color: '#E6E6E6'
                    }
                },
                boundaryGap: '10%'  //'10%|false'
            },
            yAxis: {
                show: true,
                axisLine: {
                    lineStyle: {
                        color: '#BBBBBB'
                    }
                },
                axisLabel: {
                    show: true,
                    textStyle: {
                        color: '#888888'
                    }
                },
                splitNumber: 2,
                // max: 100
            },
            series: series,
            color: this.props.lineColor
        }
    };

    render() {
        return (
            <div id={this.props.id} className="echartsLine"
                 style={{width: "calc(100% - 17px)", minHeight: this.props.minHeight, margin: '0px auto'}}></div>

        );
    }
}

function getNowFormatDate() {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    let getHours = date.getHours()
    let getMinutes = date.getMinutes()
    let getSeconds = date.getSeconds()
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    if (getHours >= 1 && getHours <= 9) {
        getHours = "0" + getHours;
    }
    if (getMinutes >= 0 && getMinutes <= 9) {
        getMinutes = "0" + getMinutes;
    }
    if (getSeconds >= 0 && getSeconds <= 9) {
        getSeconds = "0" + getSeconds;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + getHours + seperator2 + getMinutes
        + seperator2 + getSeconds;
    return currentdate;
}