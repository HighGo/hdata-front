import React from 'react';


import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import * as constants from '../constants/Constants';

let color = ['#BBBBBB','#E96A03','#45B0CB','#2B99F0','#DD1A1A',
    '#F79D44','#E9AD54','#4A87C4','#5CCBE7','#4FB38F',
    '#AADA8D','#6292E1','#8F68AC','#77BB4A',];
export default class ColumnMap extends React.Component {
    componentDidMount = () => {
        let myChart = echarts.init(document.getElementById(this.props.id));

        // console.log(this.props.monitorData)
        let options = this.setPieOption(this.props.monitorData);

        myChart.setOption(options);
        window.addEventListener("resize", this.handler, false);
    };
    componentWillUnmount() {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.dispose();
        }
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {//window event
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.setOption(myChart.getOption);
            myChart.resize();
        }
    };
    setPieOption = (data) => {
        let that = this;
        let series = [];
        let tempText = [];
        for (let i in data.loadLineList) {

            tempText = data.loadLineList[i].text;

            // if (i === '1') {
            //     tempText = [{value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}];
            // } else if (i === '2') {
            //     tempText = [{value: 22}, {value: 44}, {value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}];
            // } else if (i === '5') {
            //     tempText = [{value: 22}, {value: 44}, {value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}];
            // }
            series.push({
                name: data.loadLineList[i].name,
                type: 'bar',
                stack: 'column',
                data: tempText
                // data: [120, 132, 101, 134, 90, 230, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210]

            })
        }
        // console.log(series)

        return {
            tooltip: {
                trigger: 'item',
                axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                    type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                },
                formatter: function (param) {
                    // console.log(param);
                    return [
                        param.name + '<hr size=1 style="margin: 3px 0">',
                        param.marker + convertName(param.seriesName) + ' :   ' + param.data.value + '<br/>'
                    ].join('');
                }
            },
            legend: {
                show: true,
                selectedMode: false,
                left: 'right', // 'center' | 'left' | {number},
                top: 'middle', // 'center' | 'bottom' | {number}
                // right:'20px',
                // width:'10px',
                orient: 'vertical',//，horizontal|vertical
                align: 'left',
                padding: [5, 5, 5, 10],
                backgroundColor: 'white',
                borderColor: '#ccc',
                borderWidth: 1,
                itemWidth: 10,
                itemHeight: 10,
                formatter: function (name) {
                    return convertName(name);
                },
                data: data.legend
                // data: ['Other', 'Application', 'Configuration', 'Administrative', 'Concurrency', 'Commit', 'Idle', 'Network', 'User I/O', 'System I/O', 'Scheduler', 'Cluster', 'Queueing']
            },

            grid: {
                show: true,
                top: 14,
                left: '20',
                // bottom:'100',
                width: '91%',
                height: '88%',
                borderColor: '#ccc',
                backgroundColor: 'white',
                borderWidth: 0,
                containLabel: true
            },
            xAxis: [
                {
                    type: 'category',
                    // data: ['14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00',
                    //     '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00',
                    //     '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00', '14:01:00'
                    // ]
                    data: data.xdata,
                    axisLabel: {
                        show: true,
                        interval: 0,    // {number}
                        rotate: 24,
                        textStyle: {
                            // color: 'blue',
                            // fontFamily: 'sans-serif',
                            fontSize: 12,
                            fontStyle: 'italic',
                            // fontWeight: 'bold'
                        },
                        margin: 10,
                        formatter: function (value, index) {
                            return value.substring(11);
                        }
                        // inverse:true
                    },
                }
            ],
            yAxis: [
                {
                    type: 'value'
                }
            ],
            series: series,
            color:color
            // series: [
            //     {
            //         name: 'Other',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [120, 132, 101, 134, 90, 230, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210, 210]
            //     },
            //     {
            //         name: 'Application',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [220, 182, 191, 234, 290, 330, 310]
            //     },
            //     {
            //         name: 'Configuration',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Administrative',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Concurrency',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Commit',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Idle',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Network',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'User I/O',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'System I/O',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //
            //     {
            //         name: 'Scheduler',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Cluster',
            //         type: 'bar',
            //         stack: '广告',
            //         data: [150, 232, 201, 154, 190, 330, 410]
            //     },
            //     {
            //         name: 'Queueing',
            //         type: 'bar',
            //         stack: '广告',
            //         data: []
            //     }
            // ]
        }
    };

    render() {
        return (
            <div id={this.props.id} style={{width: "99%", height: this.props.height}}></div>
        );
    }
}

function convertName(name) {
    if (name === constants.DBINDEX_MONITOR_OTHER) {
        name = constants.DBINDEX_MONITOR_OTHER_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_APPLICATION) {
        name = constants.DBINDEX_MONITOR_APPLICATION_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_CONFIGURATION) {
        name = constants.DBINDEX_MONITOR_CONFIGURATION_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_ADMINISTRATIVE) {
        name = constants.DBINDEX_MONITOR_ADMINISTRATIVE_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_CONCURRENCY) {
        name = constants.DBINDEX_MONITOR_CONCURRENCY_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_COMMIT) {
        name = constants.DBINDEX_MONITOR_COMMIT_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_IDLE) {
        name = constants.DBINDEX_MONITOR_IDLE_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_NETWORK) {
        name = constants.DBINDEX_MONITOR_NETWORK_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_USERIO) {
        name = constants.DBINDEX_MONITOR_USERIO_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_SYSTEMIO) {
        name = constants.DBINDEX_MONITOR_SYSTEMIO_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_SCHEDULER) {
        name = constants.DBINDEX_MONITOR_SCHEDULER_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_CLUSTER) {
        name = constants.DBINDEX_MONITOR_CLUSTER_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_QUEUEING) {
        name = constants.DBINDEX_MONITOR_QUEUEING_TEXT;
    } else if (name === constants.DBINDEX_MONITOR_CPU) {
        name = constants.DBINDEX_MONITOR_CPU_TEXT;
    }
    return name;
}

