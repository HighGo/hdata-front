import React from "react";
import {Router , Route} from "react-router-dom";
import createHashHistory from "history/createHashHistory";
import HLogin from "./HLogin"
import Main from "./Main"
import * as Constants from "./constants/Constants";

const history = createHashHistory();

history.listen(location => {
        if(location.pathname !== "/"){
            console.log('listen....');
            console.log(history)
            if(localStorage.getItem("HData_UserName") === null || localStorage.getItem("HData_UserName") === undefined){
                history.push("/");
            }
        }
    });

class HdataRouter extends React.Component{
    constructor(){
        super();

        console.log('HdataRouter........')
        console.log(localStorage.getItem("HData_UserName"))
        console.log(history);
        if(localStorage.getItem("HData_UserName") === null || localStorage.getItem("HData_UserName") === undefined){
            if(history.location.pathname !== '/'){
                history.push("/");
            }
        }else{
            if(history.location.pathname === '/'){
                history.push(Constants.LINK_INDEX);
            }
        }
    }

    render(){
        return (
            <Router  history={history}>
                <div>
                    <Route exact path="/" component={HLogin}/>
                    <Route path={Constants.LINK_INDEX} component={Main}/>
                </div>
            </Router >
        )
    }
}
export default HdataRouter;