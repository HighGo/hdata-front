import React from 'react'

import PasswordModal from './PwdForm';

export default class PwdView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false
        };
    };
    setModalVisible = (modalVisible) => {
        this.setState({
            modalVisible: modalVisible
        });
    };
    render() {
        return (
            <PasswordModal modalVisible={this.state.modalVisible} setModalVisible={this.setModalVisible}/>
        )
    }
}

