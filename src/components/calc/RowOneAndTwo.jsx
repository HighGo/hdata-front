import React from 'react';
import Title from '../common/Title';
import * as constants from '../constants/Constants';

import Load from './LoadAverage';
import DeviceStatus from './DeviceStatus';
import Line from './Line';




export default class RowOneAndTwo extends React.Component{
    render(){
        return(
            <div>
                <div className="first">
                    <div className="load">
                        <Title title={constants.CALC_LOAD_AVERAGE}/>
                        <Load url={this.props.loadURL} id="CALC_LOAD_AVERAGE" minHeight="115px"/>
                    </div>
                    <div className="title">
                        <DeviceStatus url={this.props.configURL}/>
                    </div>
                </div>
                <div className="first second">
                    <div className="load">
                        <Title title={constants.CPU_STATUS}/>
                        <Line url={this.props.cpuURL} id="CPU_STATUS" minHeight="115px"/>
                    </div>
                    <div className="title">
                        {
                            this.props.list.map(function (item) {
                                return (
                                    <div className="item-list" key={item.id}>
                                        <Title title={item.title}/>
                                        <Line url={item.url} id={item.id} minHeight={item.minHeight}/>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
            </div>
        )
    }
}