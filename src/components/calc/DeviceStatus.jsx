import React from 'react';
import reqwest from 'reqwest';


export default class DeviceStatus extends React.Component{
    constructor(props){
        super(props);

        this.state={
            url:this.props.url,
            configuration:'',
            run_time:'00:00',
            days:'0',
        }
    };
    componentDidMount() {

        // console.log(this.props.url)
        this.fetch({
            url: this.state.url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.tick(),
            1000
        );
    };
    componentWillReceiveProps(nextProps) {
        this.setState({url:nextProps.url,configuration:'',run_time:'00:00', days:'0'});
        this.fetch({
            url: nextProps.url,
            method: 'get'
        });
    };
    // shouldComponentUpdate(nextProps, nextState){
    //     //if again render
    //     console.log('shouldComponentUpdate')
    //     return true;
    // }
    componentWillUnmount() {
        clearInterval(this.timerID);
    };
    tick(){
        if(this.state.configuration === ''){
            return null;
        }
        let total = (new Date(this.state.configuration.systemCurrentTime) - new Date(this.state.configuration.time))/1000;

        let days = parseInt(total/(24*60*60));
        var afterDay = total - days*24*60*60;//取得算出天数后剩余的秒数
        var hour = parseInt(afterDay/(60*60));//计算整数小时数
        var afterHour = total - days*24*60*60 - hour*60*60;//取得算出小时数后剩余的秒数
        var min = parseInt(afterHour/60);//计算整数分
        // var afterMin = total - days*24*60*60 - hour*60*60 - min*60;//取得算出分后剩余的秒数


        hour = hour < 10?'0'+hour:hour;
        min = min < 10?'0'+min:min;
        this.setState({
            days:days,
            run_time:hour + ':' + min
        })
    };
    fetch = (params = {}) => {
        reqwest({
            url: params.url
            ,method: params.method
            ,type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp)
            if(resp.success){
                this.setState({
                    configuration: resp.data
                });
            }else{
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    render(){
        // console.log('config--render--'+this.props.url)
        const size = this.state.configuration===''?'':(this.state.configuration.memory/1024).toFixed(2)+'GB';
        return(
            <div className="ComputerConfiguration">
                <div className="configuration">
                    <div className="font-blue" style={{marginTop:'15px'}}>
                        <span>{this.state.configuration.name}</span>
                        &nbsp;&nbsp;
                        <span>{this.state.configuration.address}</span>
                    </div>
                    <p>上次启动：{this.state.configuration.time}</p>

                    <div className="font-blue" style={{marginTop:'15px'}}>
                        {this.state.configuration.system}
                    </div>
                    <p>CPU：{this.state.configuration.cpu}</p>
                    <p style={{marginBottom:'10px'}}>内存：{size} </p>
                </div>
                <div className="run-time">
                    <span>{this.state.days}<span style={{marginLeft:'3px',fontSize: '17px',fontWeight: '900'}}>天</span>&nbsp;{this.state.run_time}</span>
                </div>
            </div>
        )
    }
}
function getNowFormatDate() {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + date.getHours() + seperator2 + date.getMinutes()
        + seperator2 + date.getSeconds();
    return currentdate;
}