import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';

export default class StoreNodeTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            table: []
        }
    }

    componentDidMount() {
        this.init();
    }
    componentWillReceiveProps(nextProps) {
        clearInterval(this.timerID);
        this.init(nextProps.url);
    };
    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    init() {
        // console.log('init');
        this.fetch();

        this.timerID = setInterval(
            () => this.fetch({}),
            Properties.getLoadAverageByDeviceIdURL_TIME
        );
    }

    timeOut() {
        this.fetch();
    }

    fetch() {
        // console.log(Properties.getMultipathStatusURL + this.props.id);
        let that = this;
        reqwest({
            url: Properties.getMultipathStatusURL + this.props.id
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp){
                console.log(resp)
                if (resp.success) {
                    that.setState({table: resp.dataList})
                }
            }, error(resp){
                console.log(resp)
            }
        })
    }

    render() {
        const columns = [
            {name: "ID", width: '20%'}, {name: "磁盘名称", width: '20%'}, {name: "磁盘大小(GB)", width: '25%'},
            {name: "存储主机", width: '20%'}, {name: "状态", width: '15%'}
        ];
        // const dataSource = [
        //     {
        //         id: "aljdafdslkjfasdof",
        //         text1: [{name: "sdb", size: 800}, {name: "sdc", size: 200}],
        //         primary: "data01",
        //         text2: [{status: "Normal"}, {status: "Normal"}],
        //     },
        //     {
        //         id: "zzfadsadaafdslkjf",
        //         text1: [{name: "sde", size: 800}, {name: "sdf", size: 200}],
        //         primary: "data02",
        //         text2: [{status: "Normal"}, {status: "Normal"}],
        //     },
        // ];

        return (
            <table className="store-table" cellSpacing={0} cellPadding={0}>
                <thead>
                <tr>
                    {
                        columns.map(function (item, index) {
                            return (
                                <th width={item.width} key={index}>{item.name}</th>
                            )
                        })
                    }
                </tr>
                </thead>
                <tbody>
                {
                    this.state.table.map(function (item, index) {
                        return (
                            <tr key={index}>
                                <td colSpan={3}>
                                    <table cellPadding={0} cellSpacing={0}>
                                        <tbody>
                                        {
                                            item.data.map(function (a, index) {
                                                let id = a.id;
                                                if (id.length > 6) {
                                                    id = id.substring(0, 6) + '...';
                                                }
                                                return (
                                                    <tr key={index}>
                                                        <td width="30.9%">
                                                            <div style={{overflow: 'hidden'}}>
                                                                <a title={a.id} style={{color: '#666'}}
                                                                   target="javascript:void(0)">{id}</a>
                                                            </div>
                                                        </td>
                                                        <td width="30.9%">
                                                            {a.name}
                                                        </td>
                                                        <td width="40%">
                                                            {a.size}
                                                        </td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </td>
                                <td style={{borderBottom: '1px solid #e6e6e6'}}>{item.hostName}</td>
                                <td>
                                    <table cellPadding={0} cellSpacing={0}>
                                        <tbody>
                                        {
                                            item.status.map(function (a, index) {
                                                return (
                                                    <tr key={index}>
                                                        <td>{a}</td>
                                                    </tr>
                                                )
                                            })
                                        }
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        )

    }
}