import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';


export default class DeviceName extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: 'HOSTNAME'
        }
    };
    componentDidMount = () => {//did mount
        this.fetch({
            url: Properties.getDeviceStatusURL + this.props.deviceId
            , method: 'get'
        });
    };
    componentWillReceiveProps(nextProps) {
        this.fetch({
            url: Properties.getDeviceStatusURL + nextProps.deviceId
            , method: 'get'
        });
    };
    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            // console.log(resp)
            if (resp.success) {
                this.setState({ name: resp.data.name});
            } else {
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    render() {
        return (
            <span className="title">{this.state.name}</span>
        )
    }
}