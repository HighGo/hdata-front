import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import EchartsUtils from '../common/EchartsUtils';

import echarts from 'echarts/lib/echarts';
import  'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/title';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markLine';


var colorList = [
    '#E3FAF2', '#D9F9EE', '#C6F6E5', '#B4F3DD',
    '#A1EFD4', '#8EECCB', '#7BE9C3', '#5FE5B6', '#43E0A9'
];
import * as json from '../../data/echarts.json';

//default width 1300
let margin = -65;
let length = 9;
let grid_left = '20%';

function initWidth() {
    if (document.body.clientWidth >= 1900) {
        grid_left = '15%';
        margin = -70;
        length = 11;
    } else if (document.body.clientWidth >= 1500) {
        margin = -65;
        length = 10;
    } else {
        margin = -65;
        length = 9;
        grid_left = '20%';
    }
    // console.log(document.body.clientWidth)
}
initWidth();

//mount point use
export default class MountPointUseBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            mountPointUseBar: [],
        }
    };

    componentDidMount = () => {
        this.init(this.props.url);
    };

    componentWillReceiveProps(nextProps) {
        clearInterval(this.timerID);
        const domLine = document.getElementById(this.props.id);
        //if domLine is null,echarts is not init
        if (domLine !== null) {
            let myChart = echarts.getInstanceByDom(domLine);
            if (myChart !== undefined) {
                //dispose
                myChart.dispose();
                //remove resize
                window.removeEventListener("resize", this.handler, false);
            }
        }
        this.init(nextProps.url);
    };

    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID,echarts,this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp)
            if (resp.success) {
                this.setState({
                    mountPointUseBar: resp.data
                });
                echarts.getInstanceByDom(document.getElementById(this.props.id)) == undefined ? this.initPie() : this.dynamic();
            } else {
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    init = (url) => {
        this.fetch({
            url: url,
            method: 'get'
        });
        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getMountPointUseURL_TIME
        );
    };
    initPie = () => {
        let myChart = echarts.init(document.getElementById(this.props.id));
        let option = this.setPieOption(this.state.mountPointUseBar);
        myChart.setOption(option);
        window.addEventListener("resize", this.handler, false);
    };
    handler = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            let option = myChart.getOption();
            initWidth();
            // this.dynamic();
            // console.log(grid_left)
            option.grid[0].left = grid_left;
            option.yAxis[0].axisLabel.margin = margin;
            myChart.setOption(option);
            myChart.resize();
        }
    };
    dynamic = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let option = myChart.getOption();

        let ydata = this.state.mountPointUseBar.ydata;
        let xdata = this.state.mountPointUseBar.xdata;
        let result = [];
        let series = [];
        ydata.forEach(function (item, index) {
            let str = xdata[index] + "GB";
            let strLength = str.length;
            if (length > strLength) {
                for (let i = 0; i <= (length - strLength); i++) {
                    str += "  ";
                }
            }
            result.push(str + item);
        });
        this.state.mountPointUseBar.series.forEach(function (item, index) {
            series.push(item * 100);
        });
        option.series[0].data = this.state.mountPointUseBar.series;
        option.yAxis[0].data = result;
        option.series[0].data = series;
        myChart.setOption(option);
    };
    setPieOption = (data) => {

        let series = [];
        let ydata = data.ydata;
        let xdata = data.xdata;
        let result = [];

        ydata.forEach(function (item, index) {
            let str = xdata[index] + "GB";
            let strLength = str.length;
            if (length > strLength) {
                for (let i = 0; i <= (length - strLength); i++) {
                    str += "  ";
                }
            }
            result.push(str + item);
        });
        data.series.forEach(function (item, index) {
            series.push(item * 100);
        });

        return {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'        //'line' | 'shadow'
                },
                formatter: '{a}  : {c}  %'
            },
            grid: {
                show: false,
                top: '15',
                left: grid_left,
                // right: '4%',
                // bottom: '3%',
                width: '98%',
                height: '98%',
                containLabel: true
            },
            xAxis: {
                show: false,
                type: 'value',
                max: 100,
                // data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
            },
            yAxis: {
                show: true,
                type: 'category',
                position: 'left',
                // offset: -6,
                axisLine: {show: false},
                axisTick: {show: false},
                // data: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
                data: result,
                axisLabel: {
                    // formatter: '{value}',
                    margin: margin,
                    textStyle: {
                        // align:'left'
                    },
                    inside: true,
                },
                z: 9999999
            },
            series: {
                name: '使用率',
                type: 'bar',
                label: {
                    normal: {
                        show: false,
                    }
                },
                itemStyle: {
                    normal: {
                        //每个柱子的颜色即为colorList数组里的每一项，如果柱子数目多于colorList的长度，则柱子颜色循环使用该数组
                        color: function (params) {
                            return colorList[params.dataIndex];
                        }
                    }
                },
                // data: [200, 170, 240, 244, 200, 220, 210]
                data: series
            },
            // color:colorList
        }
    };

    render() {
        return (
            <div className="mount-point-use">
                <div id={this.props.id}
                     style={{width: "94%", minHeight: this.props.minHeight, margin: '0px auto'}}></div>
            </div>

        );
    }
}