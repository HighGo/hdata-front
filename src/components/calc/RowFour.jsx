import React from 'react';
import Title from '../common/Title';

import Line from './Line';
import SearchTable from '../common/WatchTable';


export default class RowFour extends React.Component {
    render() {
        return (
            <div>
                <div className="first second">
                    <div className="load">
                        <Title title={this.props.oneTitle}/>
                        <Line url={this.props.oneURL}
                              id="PUBLIC"
                              minHeight="115px"
                        />
                    </div>
                    <div className="title">
                        {
                            this.props.list.map(function (item) {
                                return (
                                    <div className="item-list" key={item.id}>
                                        <Title title={item.title}/>
                                        <Line url={item.url} id={item.id} minHeight={item.minHeight}/>
                                    </div>
                                );
                            })
                        }
                    </div>
                </div>
                <div className="table">
                    <SearchTable url={this.props.logURL} id={this.props.id}/>
                </div>
            </div>
        )
    }
}