import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import EchartsUtils from '../common/EchartsUtils';

import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';


var colorList = [
    '#8dcb49', '#54b7cf', '#f8963d'
];

export default class LoadAverage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loadAverage: [],
        }
    };

    componentDidMount = () => {
        this.init(this.props.url, this.props.id)
    };

    componentWillReceiveProps(nextProps) {//will receive
        clearInterval(this.timerID);
        const domLine = document.getElementById(this.props.id);
        //if domLine is null,echarts is not init
        if (domLine !== null) {
            let myChart = echarts.getInstanceByDom(domLine);
            if (myChart !== undefined) {
                //dispose
                myChart.dispose();
                //remove resize
                window.removeEventListener("resize", this.handler, false);
            }
        }
        //reset
        this.init(nextProps.url, nextProps.id);
    };

    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID, echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        EchartsUtils.handler(echarts, this.props.id);
    };

    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp)
            if (resp.success) {
                this.setState({
                    loadAverage: resp.data
                });
                echarts.getInstanceByDom(document.getElementById(this.props.id)) == undefined ? this.initPie() : this.dynamic();
            } else {
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    init = (url, id) => {
        this.fetch({
            url: url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getLoadAverageByDeviceIdURL_TIME
        );
    };
    initPie = () => {//init
        let myChart = echarts.init(document.getElementById(this.props.id));
        let options = this.setPieOption(this.state.loadAverage);
        myChart.setOption(options);

        window.addEventListener("resize", this.handler, false);
    };
    dynamic = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let options = this.setPieOption(this.state.loadAverage);
        myChart.setOption(options);
    };
    setPieOption = (data) => {
        return {
            grid: {
                show: true,
                top: 'bottom',
                left: 'left',
                width: '88%',
                height: '80%',
                borderColor: '#ccc',
                borderWidth: 0,
                containLabel: true
            },
            tooltip: {
                show: true,
            },
            xAxis: {
                axisLabel: {
                    show: true,
                    interval: 0,    // {number}
                    // rotate: 20,
                    textStyle: {
                        // color: 'blue',
                        // fontFamily: 'sans-serif',
                        fontSize: 12,
                        fontStyle: 'italic',
                        // fontWeight: 'bold'
                    },
                    margin: 10
                },
                show: true,
                type: 'category',
                // data: ['1分钟', '5分钟', '15分钟'],
                data: data.xdata
            },
            yAxis: {
                show: false,
                // max: 100
            },
            series: {
                type: 'bar',
                name: '',
                // data: [20, 30, 40],
                data: data.series,
                barWidth: '60%',
                itemStyle: {
                    normal: {
                        color: function (params) {
                            return colorList[params.dataIndex];
                        }
                    },
                    //The mouse is suspended
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label: {
                    normal: {
                        show: true,
                        formatter: '{c}%',
                        position: 'top'
                    }
                },
            },
            color: ['#ffbf00', '#7265e6']
        }
    };

    render() {
        return (
            <div id={this.props.id} style={{width: "99%", minHeight: '115px'}}></div>
        );
    }
}



