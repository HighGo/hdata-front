import React from 'react';
import Properties from '../constants/Properties';
import Title from '../common/Title';
import * as constants from '../constants/Constants';

import PidBarGraph from './PidBarGraph';
import MountPointUseBar from './MountPointUseBar';
import RowOneAndTwo from './RowOneAndTwo';
import RowFour from './RowFour';
import StoreNodeTable from './StoreNodeTable';


export default class Monitor extends React.Component {
    render() {
        const id = this.props.deviceId;

        const list1 = [
            {
                "title": constants.MEMORY_STATUS,
                "id": "MEMORY_STATUS",
                "minHeight": "115px",
                "url": Properties.getMemoryStatusURL + id,
            },
            {
                "title": constants.IO_STATUS,
                "id": "IO_STATUS",
                "minHeight": "115px",
                "url": Properties.getIOLoadURL + id,
            }
        ];
        const list2 = [
            {
                "title": constants.PID_MEMORY,
                "id": "PID_MEMORY",
                "minHeight": "115px",
                "url": Properties.getMemoryTopPidURL + id,
            },
            {
                "title": constants.PID_IO,
                "id": "PID_IO",
                "minHeight": "115px",
                "url": Properties.getIoTopPidURL + id,
            }
        ];
        const list3 = [
            {
                "title": constants.PRIVATE,
                "id": "PRIVATE",
                "minHeight": "115px",
                "url": Properties.getNicPrivateURL + id,
            },
            {
                "title": constants.HCA,
                "id": "HCA",
                "minHeight": "115px",
                "url": Properties.getHCAURL + id,
            }
        ];
        const usageList = [
            {
                "title": constants.MountPointUse,
                "id": "MountPointUse",
                "minHeight": "220px",
                "url": Properties.getMountPointUseURL + id,
            },
            {
                "title": constants.FileNodeUse,
                "id": "b",
                "minHeight": "220px",
                "url": Properties.getFileNodeUseURL + id,
            },
        ];

        return (
            <div className="Monitor">
                <div className="left">
                    <RowOneAndTwo list={list1}
                                  loadURL={Properties.getLoadAverageByDeviceIdURL + id}
                                  configURL={Properties.getDeviceStatusURL + id}
                                  cpuURL={Properties.getCpuStatusURL + id}
                    />
                    <div className="first second">
                        <div className="load">
                            <Title title={constants.PID_CPU}/>
                            <PidBarGraph url={Properties.getCpuTopPidURL + id} id="PID_CPU" minHeight="115px"/>
                        </div>
                        <div className="title">
                            {
                                list2.map(function (item,index) {
                                    return (
                                        <div className="item-list" key={item.id}>
                                            <Title title={item.title}/>
                                            <PidBarGraph url={item.url} id={item.id} minHeight={item.minHeight} index={index}/>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                    <RowFour list={list3} oneTitle={constants.PUBLIC}
                             oneURL={Properties.getNicPublicURL + id}
                             logURL={Properties.getMonitorLoginfoURL}
                             id={id}
                    />

                </div>
                <div className="right">
                    {
                        usageList.map(function (item,index) {
                            return (
                                <div className="item-list" key={index}>
                                    <Title title={item.title}/>
                                    <MountPointUseBar id={item.id} url={item.url} minHeight={item.minHeight}/>
                                </div>
                            );
                        })
                    }
                    <div className="item-list">
                        <Title title={constants.StorageNode} />
                        <StoreNodeTable id={id}/>
                    </div>
                </div>
            </div>
        )
    }
}
