import React from 'react';

import '../../css/calc.css'
import history from '../../img/icon-history.png';
import monitor from '../../img/icon-monitor.png';

import Monitor from './Monitor';
import History from '../common/History'
import DeviceName from './DeviceName'

import Properties from '../constants/Properties';

export default class CalcIndex extends React.Component {
    constructor(props) {
        super(props);
        let match = this.props.match.params;
        this.state = {
            deviceId: match.id,
            tab: 1,
        }
    };

    componentWillReceiveProps(nextProps) {
        this.setState({
            deviceId: nextProps.match.params.id,
            tab: 1
        })
    };

    shouldComponentUpdate = (nextProps, nextState) => {
        console.log('shouldComponentUpdate' );
        console.log(nextState)
        return this.props.match.params.id != nextState.deviceId || this.state.tab != nextState.tab;
    };
    handTabClick = (value, e) => {
        console.log(value);
        this.setState({tab: value})
    };

    render() {
        console.log('calc.....');
        let data = {
            url: Properties.getHistoryDayURL + this.state.deviceId,
            list: [
                {
                    id: 'a0', name: "负载", index: "loadAverageInfo",
                    graphUrl: Properties.getLoadAverageHistoryHourURL,
                    tableUrl: Properties.getLoadAverageHistoryHourGridDataURL
                },
                {
                    id: 'a1', name: "CPU", index: "cpuInfo",
                    graphUrl: Properties.getCpuHistoryHourURL,
                    tableUrl: Properties.getCpuHistoryHourGridDataURL
                },

                {
                    id: 'a2', name: "内存", index: "memoryInfo",
                    graphUrl: Properties.getMemoryHistoryHourURL,
                    tableUrl: Properties.getMemoryHistoryHourGridDataURL
                },
                {
                    id: 'a3', name: "磁盘", index: "ioInfo",
                    graphUrl: Properties.getIOHistoryHourURL,
                    tableUrl: Properties.getIoHistoryHourGridDataURL
                },
                {
                    id: 'a4', name: "网络", index: "nicInfo",
                    graphUrl: Properties.getNicHistoryHourURL,
                    tableUrl: Properties.getNicHistoryHourGridDataURL
                },
                {
                    id: 'a5', name: "HCA卡", index: "hcaInfo",
                    graphUrl: Properties.getHcaHistoryHourURL,
                    tableUrl: Properties.getHcaHistoryHourGridDataURL
                }
            ]
        };

        return (
            <div className="calc">
                <div className="header">
                    <DeviceName deviceId={this.state.deviceId}/>
                    <div style={{marginLeft: '2%'}}
                         className={this.state.tab === 1 ? 'hdata-tabs-tab hdata-tabs-tab-active' : 'hdata-tabs-tab'}
                         onClick={this.handTabClick.bind(this, 1)}>
                        <img alt="" src={monitor} className=""/>实时监控
                    </div>
                    <div className={this.state.tab === 2 ? 'hdata-tabs-tab hdata-tabs-tab-active' : 'hdata-tabs-tab'}
                         onClick={this.handTabClick.bind(this, 2)}>
                        <span><img alt="" src={history}/>历史数据</span>
                    </div>
                </div>

                {this.state.tab === 1 ? <Monitor deviceId={this.state.deviceId}/> :
                    <History deviceId={this.state.deviceId} list={data} type={1}/>}
            </div>
        )
    }
}

