import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import EchartsUtils from '../common/EchartsUtils';

import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';


// bar graph

export default class PidBarGraph extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pidBar: [],
            id: this.props.id
        }
    };

    componentDidMount = () => {
        this.init(this.props.url);
    };

    componentWillReceiveProps(nextProps) {
        console.log('PidBarGraph.....componentWillReceiveProps.............')
        clearInterval(this.timerID);
        const domLine = document.getElementById(this.props.id);
        //if domLine is null,echarts is not init
        if (domLine !== null) {
            let myChart = echarts.getInstanceByDom(domLine);
            if (myChart !== undefined) {
                //dispose
                myChart.dispose();
                //remove resize
                window.removeEventListener("resize", this.handler, false);
            }
        }
        //reset
        this.init(nextProps.url);
    };

    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID, echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        EchartsUtils.handler(echarts, this.props.id);
    };
    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp)
            if (resp.success) {
                this.setState({
                    pidBar: resp.data
                });
                echarts.getInstanceByDom(document.getElementById(this.props.id)) == undefined ? this.initPie() : this.dynamic();
            } else {
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    init = (url) => {//init data and timer
        this.fetch({
            url: url,
            method: 'get'
        });
        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getNicPublicURL_TIME
        );
    };
    initPie = () => {//init granh
        let myChart = echarts.init(document.getElementById(this.props.id));
        let options = this.setPieOption(this.state.pidBar);

        myChart.setOption(options)
        window.addEventListener("resize", this.handler, false);
    };
    dynamic = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let option = myChart.getOption();

        option.series[0].data = this.state.pidBar.pidPileList;
        option.xAxis[0].data = this.state.pidBar.xdata;

        myChart.setOption(option);
    };
    setPieOption = (data) => {
        let that = this;
        return {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                },
                textStyle: {
                    align: 'left'
                },
                position: ['20%', -70],
                formatter: function (param) {
                    param = param[0];
                    let nbsp = '';

                    let s = '';
                    if (that.props.id !== 'PID_IO') {
                        nbsp = '&nbsp;%';
                        s = '-' + param.data.name + ':&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.value + nbsp + '<br/>';
                    }

                    if (param.data.name1 !== undefined && param.data.name1 !== null) {
                        s = s + '-' + param.data.name1 + ':&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.value1 + nbsp + '<br/>';
                    }
                    ;
                    if (param.data.name2 !== undefined && param.data.name2 !== null) {
                        s = s + '-' + param.data.name2 + ':&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.value2 + nbsp + '<br/>';
                    }
                    ;
                    return [
                        param.data.time + '<hr size=1 style="margin: 3px 0">',
                        s,
                        '-command:&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.command + '<br/>',
                        '-pid:&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.pid + '<br/>'
                    ].join('');
                }
            },
            legend: {
                show: false,
            },
            grid: {
                show: true,
                top: '10',
                left: '20',
                width: '75%',
                height: '90%',
                borderColor: '#ccc',
                borderWidth: 0,
                containLabel: false
            },
            xAxis: {
                show: false,
                type: 'category',
                // data: ['sshd', 'fxp', 'agent', '', ''],
                data: data.xdata,
                axisLabel: {
                    textStyle: {
                        color: '#666',
                        fontSize: 14
                    }
                }
            },
            yAxis: {
                type: 'value',
                show: false,
                // max: 100
            },
            series: [{
                name: '',
                type: 'bar',
                stack: 'value',
                label: {
                    normal: {
                        position: 'insideRight'
                    }
                },
                // barWidth:28,
                // barMaxWidth:40,
                // barGap:'40%',
                // data: [{value: 80, name: '%CPU', pid: '8833', time: '2016-01-01',command:'sshd1'}
                // ,{value: 76, name: '%CPU', pid: '8833', time: '2016-01-01',command:'sshd2'}
                // ,{value: 45, name: '%CPU', pid: '8833', time: '2016-01-01',command:'sshd3'}
                // ,{value: 22, name: '%CPU', pid: '8833', time: '2016-01-01',command:'sshd4'}
                // ,{value: 10, name: '%CPU', pid: '8833', time: '2016-01-01',command:'sshd5'}],
                data: data.pidPileList

            }],
            color: ['#ED5555']
        }
    };

    render() {
        return (
            <div id={this.props.id} style={{width: "99%", minHeight: this.props.minHeight}}></div>
        );
    }
}



