import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import EchartsUtils from '../common/EchartsUtils';

import echarts from 'echarts/lib/echarts';
import  'echarts/lib/chart/line';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';
import 'echarts/lib/component/markLine';



var colorList = ['#70D9C0', '#FFCE57','#EC8CC2'];

var point = Properties.cpuInterval/Properties.getAllCpuStatusURL_TIME;//echarts x points

export default class Line extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            lineData: []
        }
    };
    componentDidMount = () => {//did mount
        this.init(this.props.url)
    };
    componentWillReceiveProps(nextProps) {//will receive
        clearInterval(this.timerID);
        const domLine = document.getElementById(this.props.id);
        //if domLine is null,echarts is not init
        if(domLine !== null){
            let myChart = echarts.getInstanceByDom(domLine);
            if (myChart !== undefined) {
                //dispose
                myChart.dispose();
                //remove resize
                window.removeEventListener("resize", this.handler, false);
            }
        }
        //reset
        this.init(nextProps.url);
    };
    componentWillUnmount() {//unmount
        EchartsUtils.Unmount(this.timerID, echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };
    handler = () => {
        EchartsUtils.handler(echarts,this.props.id);
    };
    fetch = (params = {}) => {
        // console.log(params.url)
        reqwest({
            url: params.url,
            method: params.method,
            data: {
                ...params,
            }
            ,type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            if(resp.success){
                // console.log(params.url)
                // console.log(resp.data)
                console.log(resp)
                if(resp.data.xdata.length < point){
                    const c = getNowFormatDate();
                    const diff = point - resp.data.xdata.length;

                    for(let i=0; i < diff; i++){//minute point
                        resp.data.xdata.unshift(c);
                    }

                    resp.data.loadLineList.forEach(function (value, index, array) {
                        for(let i=0; i < diff; i++){
                            value.text.unshift({value: 0, time: c})
                        }
                    });
                };
                this.setState({
                    lineData: resp.data
                });
                echarts.getInstanceByDom(document.getElementById(this.props.id)) == undefined ? this.initPie() : this.dynamic();
            }else{
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    init = (url) =>{
        this.fetch({
            url: url,
            method: 'get'
        });
        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getCpuStatusURL_TIME
        );
    };
    initPie = () => {
        let myChart = echarts.init(document.getElementById(this.props.id));
        let option = this.setPieOption(this.state.lineData);
        myChart.setOption(option);

        window.addEventListener("resize", this.handler, false);
    };
    dynamic = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let option = myChart.getOption();

        let that = this.state.lineData;

        let lastTime = option.xAxis[0].data[option.xAxis[0].data.length-1];
        option.series.forEach(function (item, index) {
            let obj = that.loadLineList[index].text[that.loadLineList[index].text.length - 1];

            option.series[index].data.shift();//step option series
            if( lastTime === obj.time){
                obj.value = "NaN";
            }
            option.series[index].data.push(obj);
        });

        option.xAxis[0].data.shift();
        option.xAxis[0].data.push(this.state.lineData.xdata[this.state.lineData.xdata.length - 1]);

        myChart.setOption(option);

    };
    setPieOption = (data) => {
        // console.log(data)
        let legent = [];
        let series = [];
        let xdata = data.xdata;

        data.loadLineList.forEach(function (value, index, array) {
            if(value.show){
                series.push({
                    type: 'line',
                    name: value.name,
                    data: value.text,
                });
            }else{
                series.push({
                    type: 'line',
                    name: value.name,
                    data: value.text,
                    lineStyle:{
                        normal:{
                            width:0
                        }
                    },
                    symbolSize:0
                });
            }
        });

        data.legend.forEach(function (value, index, array) {
            if(value !== 'Tps'){
                legent.push({
                    name: value,
                    icon:'rect'
                });
            }
        });

        return {
            legend:{
                show:true,
                left:'center',
                top:'bottom',
                data:legent,
                selectedMode:false,
                itemWidth: 20,             // 图例图形宽度
                itemHeight: 8,            // 图例图形高度
            },
            tooltip: {
                position: function (pos, params, dom, rect, size) {
                    // 鼠标在左侧时 tooltip 显示到右侧，鼠标在右侧时 tooltip 显示到左侧。
                    var obj = {top: -30};
                    obj[['left', 'right'][+(pos[0] < size.viewSize[0] / 2)]] = 5;
                    return obj;
                },
                trigger: 'axis',
                // formatter: '{a0}:{b0}: {c0}<br />{a1}::{b1}: {c1}',
                axisPointer: {
                    type: 'line',
                    label: {
                        show: false,
                        backgroundColor: '#6a7985',
                        textStyle: {
                            fontSize: 20
                        },
                        // padding:[10,7,5,7]
                    },
                    // crossStyle:{
                    //     opacity:0
                    // }
                },
                textStyle: {
                    fontSize: 12,
                    align:'left'
                }
                // confine: true
            },
            grid: {
                show: true,
                top: '20',
                left: '0',
                width: '90%',
                height: '72%',
                // borderColor:'#ccc',
                borderWidth: 0,
                containLabel: true
            },
            xAxis: {
                show: false,
                type: 'category',
                data: xdata,
                // type: 'time',
                boundaryGap: '10%'  //'10%|false'
            },
            yAxis: {
                show: true,
                axisLabel: {
                    show: true
                },
                splitNumber: 2,
                // max: 100
            },
            series: series,
            color: colorList
        }
    };

    render() {
        return (
            <div id={this.props.id}
                 style={{width: "calc(100% - 17px)", minHeight: this.props.minHeight, margin: '0px auto'}}></div>
        );
    }
}

function getNowFormatDate() {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    let getHours = date.getHours()
    let getMinutes = date.getMinutes()
    let getSeconds = date.getSeconds()
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    if (getHours >= 1 && getHours <= 9) {
        getHours = "0" + getHours;
    }
    if (getMinutes >= 0 && getMinutes <= 9) {
        getMinutes = "0" + getMinutes;
    }
    if (getSeconds >= 0 && getSeconds <= 9) {
        getSeconds = "0" + getSeconds;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + getHours + seperator2 + getMinutes
        + seperator2 + getSeconds;
    return currentdate;
}