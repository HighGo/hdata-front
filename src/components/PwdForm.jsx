import React from 'react'
import reqwest from 'reqwest';
import {Modal, Form, Input} from 'antd';
const FormItem = Form.Item;

import confirm from '../img/btn-confirm.png';
import cancelIcon from '../img/btn-cancel.png';

import Properties from './constants/Properties';

var errorFlag = false;
class PwdForm extends React.Component {
    initPassword = (rule, value, callback) => {
        console.log('initPassword:::::' + value + ":;;;;" + errorFlag);
        if (errorFlag) {
            callback('密码错误');
            errorFlag = false;
        }
        callback();
    };
    checkPassword = (rule, value, callback) => {
        if (value && value !== this.props.form.getFieldValue('pwd2')) {
            callback('两次密码必须一致');
        }
        callback();
    };
    handleSubmit = (e) => {
        e.preventDefault();

        this.props.form.validateFields(['pwd3'], {force: true});

        let that = this;
        this.props.form.validateFieldsAndScroll((err, values) => {
            if (!err) {
                let pwd1 = this.props.form.getFieldValue('pwd1');
                let pwd2 = this.props.form.getFieldValue('pwd2');
                let pwd3 = this.props.form.getFieldValue('pwd3');

                const name = localStorage.getItem("HData_UserName");
                reqwest({
                    url: Properties.updatePasswordURL
                    , method: 'get'
                    , type: 'json'
                    , contentType: 'application/json'
                    , data: {
                        userName: name,
                        oldPassword: pwd1,
                        newPassword: pwd2
                    }, success(resp){
                        console.log(resp)
                        if (resp.returnCode === 1001) {
                            console.log('password is error.');
                            errorFlag = true;
                            that.props.form.validateFields(['pwd1'], {force: true});
                        } else {
                            //normal
                            that.destroy();
                        }
                    }, error(resp){
                        console.log(resp)
                    }
                });
            }
        });
    };
    destroy = () => {
        this.props.form.setFieldsValue({'pwd1': '', 'pwd2': '', 'pwd3': ''});
        this.props.setModalVisible(false)
    };

    render() {
        const {getFieldDecorator} = this.props.form;
        const formItemLayout = {
            labelCol: {
                xs: {span: 24},
                sm: {span: 6},
            },
            wrapperCol: {
                xs: {span: 24},
                sm: {span: 16},
            },
        };

        return (
            <Modal
                wrapClassName="vertical-center-modal"
                visible={this.props.modalVisible}
                closable={false}
                style={{fontSize: '18px'}}
                width="357px"
                height="300px"
            >
                <div className="password-title">修改密码</div>
                <Form hideRequiredMark={true}>
                    <FormItem
                        {...formItemLayout}
                        label="初始密码"
                        hasFeedback
                        // validateStatus={false}
                    >
                        {getFieldDecorator('pwd1', {
                            rules: [{
                                required: true, message: '*密码输入错误信息',
                            }, {
                                validator: this.initPassword
                            }],
                            initialValue: ''
                        })(
                            <Input placeholder="请输入初始密码"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="设置密码"
                        hasFeedback
                        // validateStatus={false}
                    >
                        {getFieldDecorator('pwd2', {
                            rules: [{
                                required: true, message: '*密码输入错误信息',
                            }],
                        })(
                            <Input placeholder="请输入设置密码"/>
                        )}
                    </FormItem>
                    <FormItem
                        {...formItemLayout}
                        label="确认密码"
                        hasFeedback
                        // validateStatus={false}
                    >
                        {getFieldDecorator('pwd3', {
                            rules: [{
                                required: true, message: '*密码输入错误信息',
                            }, {
                                validator: this.checkPassword
                            }],
                        })(
                            <Input placeholder="请输入确认密码"/>
                        )}
                    </FormItem>
                </Form>
                <div style={{textAlign: 'center', marginTop: '15px'}}>
                    <img alt="" src={confirm} style={{marginRight: '20px',cursor: 'pointer'}} onClick={this.handleSubmit}/>
                    <img alt="" src={cancelIcon} style={{cursor: 'pointer'}} onClick={this.destroy}/>
                </div>
            </Modal>
        )
    }
}
const WrappedRegistrationForm = Form.create()(PwdForm);
export default WrappedRegistrationForm