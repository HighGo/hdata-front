import React from 'react';
import reqwest from 'reqwest';
import Properties from './constants/Properties';
import * as Constants from './constants/Constants';

import {Spin} from 'antd';
import register from '../img/btn-register.png';

var normal = '1px solid #ddd';
var error = '1px solid red';
export default class Register extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            dbServerName: '',
            dbUser: 'SYS',
            dbPwd: '',
            dbPort:'',
            ip1: '',
            ip2: '',
            ip3: '',
            ip4: '',
            ip5: '',
            pwd1: '',
            pwd2: '',
            pwd3: '',
            pwd4: '',
            pwd5: '',
            port1: '',
            port2: '',
            port3: '',
            port4: '',
            port5: '',
            peonyPort1:'',
            peonyPort2:'',


            dbServerNameCss: normal,
            dbPwdCss: normal,
            dbPortCss: normal,
            ip1Css: normal,
            ip2Css: normal,
            ip3Css: normal,
            ip4Css: normal,
            ip5Css: normal,
            pwd1Css: normal,
            pwd2Css: normal,
            pwd3Css: normal,
            pwd4Css: normal,
            pwd5Css: normal,
            port1Css: normal,
            port2Css: normal,
            port3Css: normal,
            port4Css: normal,
            port5Css: normal,
            peonyPort1Css:normal,
            peonyPort2Css:normal,


            errorMsg: []
        }
    }

    onSubmit = () => {
        console.log('submit');

        this.setState({
            dbServerNameCss: normal,
            dbUserCss:normal,
            dbPwdCss: normal,
            ip1Css: normal,
            ip2Css: normal,
            ip3Css: normal,
            ip4Css: normal,
            ip5Css: normal,
            pwd1Css: normal,
            pwd2Css: normal,
            pwd3Css: normal,
            pwd4Css: normal,
            pwd5Css: normal,
            port1Css: normal,
            port2Css: normal,
            port3Css: normal,
            port4Css: normal,
            port5Css: normal,
            errorMsg: []
        });

        let flag = true;
        if (this.state.dbServerName.trim().length === 0) {
            this.setState({dbServerNameCss: error});
            flag = false;
        }
        if (this.state.dbUser.trim().length === 0) {
            this.setState({dbUserCss: error});
            flag = false;
        }
        if (this.state.dbPwd.trim().length === 0) {
            this.setState({dbPwdCss: error});
            flag = false;
        }
        if (this.state.ip1.trim().length === 0) {
            this.setState({ip1Css: error});
            flag = false;
        }
        if (this.state.ip2.trim().length === 0) {
            this.setState({ip2Css: error});
            flag = false;
        }
        if (this.state.ip3.trim().length === 0) {
            this.setState({ip3Css: error});
            flag = false;
        }
        if (this.state.ip4.trim().length === 0) {
            this.setState({ip4Css: error});
            flag = false;
        }
        if (this.state.ip5.trim().length === 0) {
            this.setState({ip5Css: error});
            flag = false;
        }
        if (this.state.pwd1.trim().length === 0) {
            this.setState({pwd1Css: error});
            flag = false;
        }
        if (this.state.pwd2.trim().length === 0) {
            this.setState({pwd2Css: error});
            flag = false;
        }
        if (this.state.pwd3.trim().length === 0) {
            this.setState({pwd3Css: error});
            flag = false;
        }
        if (this.state.pwd4.trim().length === 0) {
            this.setState({pwd4Css: error});
            flag = false;
        }
        if (this.state.pwd5.trim().length === 0) {
            this.setState({pwd5Css: error});
            flag = false;
        }
        if (this.state.port1.trim().length === 0) {
            this.setState({port1Css: error});
            flag = false;
        }
        if (this.state.port2.trim().length === 0) {
            this.setState({port2Css: error});
            flag = false;
        }
        if (this.state.port3.trim().length === 0) {
            this.setState({port3Css: error});
            flag = false;
        }
        if (this.state.port4.trim().length === 0) {
            this.setState({port4Css: error});
            flag = false;
        }
        if (this.state.port5.trim().length === 0) {
            this.setState({port5Css: error});
            flag = false;
        }
        if (this.state.peonyPort1.trim().length === 0) {
            this.setState({peonyPort1Css: error});
            flag = false;
        }
        if (this.state.peonyPort2.trim().length === 0) {
            this.setState({peonyPort2Css: error});
            flag = false;
        }
        if (this.state.dbPort.trim().length === 0) {
            this.setState({dbPortCss: error});
            flag = false;
        }
        if (flag) {
            let params = {"dbUser": this.state.dbUser, "dbServerName": this.state.dbServerName, "dbPwd": this.state.dbPwd,"dbPort": this.state.dbPort};
            let calc = [
                {ip: this.state.ip1, name: 'root', pwd: this.state.pwd1, port: this.state.port1,peonyPort: this.state.peonyPort1},
                {ip: this.state.ip2, name: 'root', pwd: this.state.pwd2, port: this.state.port2,peonyPort: this.state.peonyPort2}
            ];
            let storage = [
                {ip: this.state.ip3, name: 'root', pwd: this.state.pwd3, port: this.state.port3},
                {ip: this.state.ip4, name: 'root', pwd: this.state.pwd4, port: this.state.port4},
                {ip: this.state.ip5, name: 'root', pwd: this.state.pwd5, port: this.state.port5}
            ];
            params['calc'] = calc;
            params['storage'] = storage;
            console.log(JSON.stringify(params));

            this.setState({loading: true});

            let that = this;
            reqwest({
                url: Properties.registerHdataURL
                , method: 'post'
                , data: JSON.stringify(params)
                , type: 'json'
                , contentType: 'application/json'
                , success: function (resp) {
                    // resp.returnCode = 1000;
                    console.log(resp)
                    if (resp.returnCode === 1000) {
                        that.props.onChangeView(1);
                    } else {
                        const msg = [];
                        resp.data.calc.forEach(function (item, index) {
                            let error = '';
                            if (item.shhErr) {
                                error = '\t' + item.shhErrMessage;
                            }
                            if (item.agentErr) {
                                error += '\t' + item.agentErrMessage;
                            }
                            if (item.poenyErr) {
                                error += '\t' + item.poenyErrMessage;
                            }
                            if (item.infinibandErr) {
                                error += '\t' + item.infinibandErrMessage;
                            }
                            msg.push(item.ip + error);
                        });
                        resp.data.storage.forEach(function (item, index) {
                            let error = '';
                            if (item.shhErr) {
                                error = '\t' + item.shhErrMessage;
                            }
                            if (item.agentErr) {
                                error += '\t' + item.agentErrMessage;
                            }
                            msg.push(item.ip + error);
                        });
                        console.log(msg)
                        that.setState({errorMsg: msg});
                    }
                    that.setState({loading: false});
                }, error(resp) {
                    console.log(resp)
                    that.setState({loading: false});
                }
            })


        }

    };

    componentDidMount() {
        // var msg = [];
        // msg.push('192.168.100.111 \t com.jcraft.jsch.JSchException: java.net.ConnectException: Connection refused: connect \t For input string: "nul1111111111l"');
        // this.setState({errorMsg:msg})
    }

    render() {

        return (
            <div style={{height: '100vh'}} className="register">
                <h2>注册主机</h2>
                <div className="content example">
                    <Spin spinning={this.state.loading}>
                        <table className="table" cellPadding={0} cellSpacing={0}>
                            <tbody>
                            <tr>
                                <td className="left">数据库服务名：</td>
                                <td><input value={this.state.dbNdbServerNameame}
                                           onChange={(e) => this.setState({dbServerName: e.target.value})}
                                           placeholder="数据库服务名"
                                           style={{width: '200px', border: this.state.dbServerNameCss}}

                                />
                                </td>
                            </tr>
                            <tr>
                                <td className="left">用户名：</td>
                                <td><input value={this.state.dbUser}
                                           onChange={(e) => this.setState({dbUser: e.target.value})}
                                           style={{width: '200px', border: this.state.dbUserCss}}
                                />
                                </td>
                            </tr>
                            <tr>
                                <td className="left">密码：</td>
                                <td><input value={this.state.dbPwd}
                                           onChange={(e) => this.setState({dbPwd: e.target.value})}
                                           placeholder="SYS用户密码"
                                           style={{width: '200px', border: this.state.dbPwdCss}}
                                />
                                </td>
                            </tr>
                            <tr>
                                <td className="left">端口号：</td>
                                <td><input value={this.state.dbPort}
                                           onChange={(e) => this.setState({dbPort: e.target.value})}
                                           placeholder="数据库端口号"
                                           style={{width: '200px', border: this.state.dbPortCss}}
                                />
                                </td>
                            </tr>
                            <tr>
                                <td className="left" style={{paddingTop: '1%', verticalAlign: 'top'}}>计算节点：</td>
                                <td>
                                    <table className="table2" cellSpacing={0} cellPadding={0}>
                                        <tbody>
                                        <tr className="title">
                                            <td>IP</td>
                                            <td width="15%">用户名</td>
                                            <td>密码</td>
                                            <td>代理端口</td>
                                            <td>peony端口号</td>
                                        </tr>
                                        {/*1*/}
                                        <tr>
                                            <td><input value={this.state.ip1}
                                                       onChange={(e) => this.setState({ip1: e.target.value})}
                                                       style={{border: this.state.ip1Css}}
                                                       placeholder="请输入ip地址"/>
                                            </td>
                                            <td>root</td>
                                            <td><input value={this.state.pwd1}
                                                       onChange={(e) => this.setState({pwd1: e.target.value})}
                                                       style={{border: this.state.pwd1Css}} type="password"
                                                       placeholder="请输入密码"/>
                                            </td>
                                            <td><input value={this.state.port1}
                                                       onChange={(e) => this.setState({port1: e.target.value})}
                                                       style={{border: this.state.port1Css}}
                                                       placeholder="4545"/></td>
                                            <td><input value={this.state.peonyPort1}
                                                       onChange={(e) => this.setState({peonyPort1: e.target.value})}
                                                       style={{border: this.state.port1Css}}
                                                       placeholder="8888"/></td>
                                        </tr>
                                        {/*2*/}
                                        <tr>
                                            <td><input value={this.state.ip2}
                                                       onChange={(e) => this.setState({ip2: e.target.value})}
                                                       style={{border: this.state.ip2Css}}
                                                       placeholder="请输入ip地址"/></td>
                                            <td>root</td>
                                            <td><input value={this.state.pwd2}
                                                       onChange={(e) => this.setState({pwd2: e.target.value})}
                                                       style={{border: this.state.pwd2Css}} type="password"
                                                       placeholder="请输入密码"/>
                                            </td>
                                            <td><input value={this.state.port2}
                                                       onChange={(e) => this.setState({port2: e.target.value})}
                                                       style={{border: this.state.peonyPort1Css}}
                                                       placeholder="4545"/></td>
                                            <td><input value={this.state.peonyPort2}
                                                       onChange={(e) => this.setState({peonyPort2: e.target.value})}
                                                       style={{border: this.state.peonyPort2Css}}
                                                       placeholder="8888"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td className="left" style={{paddingTop: '1%', verticalAlign: 'top'}}>存储节点：</td>
                                <td>
                                    <table className="table2" cellSpacing={0} cellPadding={0}>
                                        <tbody>
                                        <tr className="title">
                                            <td width="25%">IP</td>
                                            <td width="25%">用户名</td>
                                            <td width="30%">密码</td>
                                            <td width="20%">代理端口</td>
                                        </tr>
                                        {/*3*/}
                                        <tr>
                                            <td><input value={this.state.ip3}
                                                       onChange={(e) => this.setState({ip3: e.target.value})}
                                                       style={{border: this.state.ip3Css}}
                                                       placeholder="请输入ip地址"/></td>
                                            <td>root</td>
                                            <td><input value={this.state.pwd3}
                                                       onChange={(e) => this.setState({pwd3: e.target.value})}
                                                       style={{border: this.state.pwd3Css}} type="password"
                                                       placeholder="请输入密码"/>
                                            </td>
                                            <td><input value={this.state.port3}
                                                       onChange={(e) => this.setState({port3: e.target.value})}
                                                       style={{border: this.state.port3Css}}
                                                       placeholder="4545"/></td>
                                        </tr>
                                        {/*4*/}
                                        <tr>
                                            <td><input value={this.state.ip4}
                                                       onChange={(e) => this.setState({ip4: e.target.value})}
                                                       style={{border: this.state.ip4Css}}
                                                       placeholder="请输入ip地址"/></td>
                                            <td>root</td>
                                            <td><input value={this.state.pwd4}
                                                       onChange={(e) => this.setState({pwd4: e.target.value})}
                                                       style={{border: this.state.pwd4Css}} type="password"
                                                       placeholder="请输入密码"/>
                                            </td>
                                            <td><input value={this.state.port4}
                                                       onChange={(e) => this.setState({port4: e.target.value})}
                                                       style={{border: this.state.port4Css}}
                                                       placeholder="4545"/></td>
                                        </tr>
                                        <tr>
                                            <td><input value={this.state.ip5}
                                                       onChange={(e) => this.setState({ip5: e.target.value})}
                                                       style={{border: this.state.ip5Css}}
                                                       placeholder="请输入ip地址"/></td>
                                            <td>root</td>
                                            <td><input value={this.state.pwd5}
                                                       onChange={(e) => this.setState({pwd5: e.target.value})}
                                                       style={{border: this.state.pwd5Css}} type="password"
                                                       placeholder="请输入密码"/>
                                            </td>
                                            <td><input value={this.state.port5}
                                                       onChange={(e) => this.setState({port5: e.target.value})}
                                                       style={{border: this.state.port5Css}}
                                                       placeholder="4545"/></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </Spin>
                </div>
                <div width='100%'>
                    {
                        this.state.errorMsg.map(function (item, index) {
                            return (
                                <div key={index} style={{color: '#FF4000', fontSize: '14px'}}>
                                    <pre>{item}</pre>
                                </div>
                            )
                        })
                    }
                </div>
                <div style={{textAlign: 'center', width: '100%', marginTop: '15px'}}>
                    <img alt="" src={register} onClick={this.onSubmit}/>
                </div>
            </div>
        )
    }
}