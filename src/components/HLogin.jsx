import React from "react";
import reqwest from 'reqwest';
import '../css/login.css';
import Logologin from "../img/logo-login.png"
import Properties from './constants/Properties';
import * as Constants from './constants/Constants';

export default class HLogin extends React.Component {
    constructor() {
        super();
        this.state = {
            userName: '',
            password: '',
            errMsg: ''
        }

    }

    login(event) {
        event.preventDefault();

        if (this.state.userName.trim() === '') {
            this.setState({errMsg: '用户名不能为空。'});
            return;
        }
        let that = this;
        reqwest({
            url: Properties.checkLoginUserURL
            , method: 'get'
            , type: 'json'
            , contentType: 'application/json'
            , data: {
                userName: this.state.userName,
                password: this.state.password,
            }, success(resp){
                console.log(resp);
                if (resp.returnCode === 1001) {
                    that.setState({errMsg: "用户名或密码错误"});
                } else {
                    localStorage.setItem("HData_UserName", that.state.userName);
                    that.props.history.push(Constants.LINK_INDEX);
                }
            }, error(resp){
                console.log(resp)
            }
        });
    }

    render() {
        return (
            <div className="login-bg">
                <div className="login-tbl">
                    <form onSubmit={this.login.bind(this)} method="post">
                        <img src={Logologin} alt=""/>

                        <input name="name"
                               placeholder="用户名"
                               value={this.state.userName}
                               onChange={(e) => this.setState({userName: e.target.value})}
                        />

                        <input type="password"
                               name="password"
                               placeholder="密码"
                               value={this.state.password}
                               onChange={(e) => this.setState({password: e.target.value})}
                        />
                        <button className="login-btn" type="submit">登录</button>
                    </form>

                    <span className="login-err"><strong>{this.state.errMsg}</strong></span>
                </div>
            </div>
        )
    }
}
