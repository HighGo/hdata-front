import React from 'react';
import LogTop from '../img/logo-top.png';


import PwdView from './PwdView';
import MainContent from './MainContent';
import Register from './Register';


import {Layout} from 'antd';

const {Header} = Layout;

import reqwest from 'reqwest';
import Properties from './constants/Properties';
import * as Constants from './constants/Constants';
import {createHashHistory} from "history";

const history = createHashHistory();

export default class Main extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            id: 'sub1',
            view: 1,//1:watch,2:register
            menu: [],
        };
    };

    componentDidMount = () => {
        reqwest({
            url: Properties.getMenuListURL
            , method: 'get'
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            // resp.dataList = null;//test go register
            // console.log(resp);
            if (resp.success) {
                if (resp.dataList === null || resp.dataList.length === 0) {
                    this.setState({view: 2});//go register
                } else {
                    // this.setState({menu: resp.dataList});

                    //init tab value
                    const path = history.location.pathname;
                    if (path === Constants.SETTING_INDEX) {
                        this.setState({id: 'sub5', menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX) {
                        this.setState({id: 'sub4', menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_ASMSIZE) {
                        this.setState({id: Constants.dbId_1, menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_ASMSTATUS) {
                        this.setState({id: Constants.dbId_2, menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_CLUSTERQUERY) {
                        this.setState({id: Constants.dbId_3, menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_TABLESPACE) {
                        this.setState({id: Constants.dbId_4, menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_RMAN) {
                        this.setState({id: Constants.dbId_5, menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_MEMORY) {
                        this.setState({id: Constants.dbId_6, menu: resp.dataList})
                    } else if (path === Constants.DB_INDEX_HISTORY) {
                        this.setState({id: Constants.dbId_7, menu: resp.dataList})
                    }else if (path === Constants.DB_INDEX_SQL) {
                        this.setState({id: Constants.dbId_8, menu: resp.dataList})
                    }else if (path === Constants.DB_INDEX_LOCK) {
                        this.setState({id: Constants.dbId_9, menu: resp.dataList})
                    } else if (path.indexOf(Constants.LINK_INDEX + '/') > -1) {
                        const number = 'menu' + path.substring(path.lastIndexOf('/') + 1);
                        this.setState({id: number, menu: resp.dataList});
                    } else {
                        this.setState({menu: resp.dataList})
                    }
                }
            } else {
                console.log(Properties.getMenuListURL);
                console.log(resp);
            }
        });
    };
    handOnChangeStatus = (value, e) => {
        this.setState({id: value})
    };
    _handOnChangeView = (value, e) => {
        console.log(value)
        this.setState({view: value});

        //if data last
        this.reqwestMenu();
    };
    reqwestMenu = () => {
        let that = this;
        reqwest({
            url: Properties.getMenuListURL
            , method: 'get'
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp);
            if (resp.success) {
                if (resp.dataList === null || resp.dataList.length === 0) {
                    //register success ,but webservice is false.so, add a timer.
                    that.timer = setInterval(() =>
                        this.reqwestMenu(), 8000)
                } else {
                    clearInterval(that.timer);
                    this.setState({menu: resp.dataList});
                }
            } else {
                console.log(Properties.getMenuListURL);
                console.log(resp);
            }
        });
    };
    PwdView = () => {
        this.refs.pwdChild.setModalVisible(true);
    };
    logout = () => {
        localStorage.removeItem("HData_UserName");
        this.props.history.push("/");
    };

    render() {
        console.log('Main......');
        //<div style={{height: '100vh'}}>
        return (

            <div style={{height: '100vh'}}>
                <PwdView ref="pwdChild"/>

                <Layout style={{backgroundColor: '#f7f7f7', height: '100%'}}>
                    <Header className="header" style={{padding: '0px 5px'}}>
                        <div className="logo">
                            <img src={LogTop} alt=""/>
                        </div>
                        <div className="exit">
                            <span className="logout" onClick={() => this.PwdView()}>修改密码</span>
                            &nbsp;&nbsp;|&nbsp;&nbsp;
                            <span className="logout" onClick={this.logout.bind(this)}>退出</span>
                        </div>
                    </Header>

                    {this.state.view === 1 ?
                        <MainContent menu={this.state.menu}
                                     onChangeStatus={this.handOnChangeStatus}
                                     id={this.state.id}
                                     view={this.state.view}
                        />
                        : <Register onChangeView={this._handOnChangeView}/>
                    }

                </Layout>
            </div>

        )
    }
}