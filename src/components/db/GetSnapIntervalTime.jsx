import React from 'react';
import moment from 'moment';
import {Select, DatePicker} from 'antd';

const Option = Select.Option;


export default class GetSnapIntervalTime extends React.Component {

    disabledDate = (current) => {
        if(current !== undefined){
            let day = current.format('YYYY-MM-DD');
            // console.log(day)
            const length = this.props.snapIntervalTime.length - 1;
            return this.props.snapIntervalTime[length].beginTime > day || this.props.snapIntervalTime[0].beginTime < day;
        }
    };
    render() {

        return (
            <tr>
                <td className="title">选择时间</td>
                <td>
                    <DatePicker
                        disabled={this.props.datePickerDisabled}
                        format="YYYY-MM-DD"
                        disabledDate={this.disabledDate}
                        onChange = {this.props.datePickerChange}
                    />

                    <Select style={{width: 230,marginLeft:'5px'}} onChange={this.props.onChange}>
                        {
                            this.props.selectDatePickerHourList.map(function (item, index) {
                                return (
                                    <Option value={item.key+''} key={index}>
                                        {item.name}
                                    </Option>
                                )
                            })
                        }
                    </Select>
                    <span style={{marginLeft:'15px'}}>{this.props.datePickerItem.endTime}</span>

                    {/*<Select style={{width: 330}} onChange={this.props.onChange}>*/}
                        {/*{*/}
                            {/*this.props.snapIntervalTime.map(function (item, index) {*/}
                                {/*return (*/}
                                    {/*<Option value={item.snapId + ''} key={item.snapId}>*/}
                                        {/*{item.beginTime} —— {item.endTime}*/}
                                    {/*</Option>*/}
                                {/*)*/}
                            {/*})*/}
                        {/*}*/}
                    {/*</Select>*/}

                    {
                        React.Children.map(this.props.children, function (child) {
                            return <button className="buttonItem buttonItemSelect"
                                           onClick={child.onClick}
                                           style={{marginLeft: '10px'}}>
                                {child}</button>;
                        })
                    }
                </td>
            </tr>
        )
    }
}