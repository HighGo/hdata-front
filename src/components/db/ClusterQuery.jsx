import React from 'react';
// import '../../css/lock.css';
import {Tables} from "./Tables";
import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import Header from './Header';

export default class ClusterQuery extends React.Component {
    render() {
        const columns = [{
            title: 'Name',
            dataIndex: 'name',
            render: (text, row, index) => {
                const obj = {
                    children: text,
                    props: {},
                };

                if (row.colSpan === true) {
                    obj.props.children = <div>{text}</div>;
                    // obj.props.colSpan = 4;
                    obj.props.className = 'column-colspan-title';
                    return obj;
                }
                if (row.rowSpan !== 1) {
                    obj.props.rowSpan = row.rowSpan;
                }

                return obj;
            },
        }, {
            title: 'TARGET',
            dataIndex: 'target',
            render: renderContent,
        },
            {
                title: 'STATE',
                dataIndex: 'state',
                render: renderContent,
            }, {
                title: 'SERVER',
                dataIndex: 'server',
                render: renderContent,
            }, {
                title: 'STATE_DETAILS',
                dataIndex: 'state_details',
                render: renderContent,
            }];
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_CLUSTERQUERY_TEXT}/>
                <div style={{width: '100%', margin: '0 auto'}}>
                    <Tables id="asm" columns={columns} url={Properties.getRACMonitorInfoURL}/>
                </div>
            </div>
        );
    }
}
const renderContent = (value, row, index) => {
    const obj = {
        children: value,
        props: {className: 'column-normal'},
    };
    // console.log(row)
    if (row.colSpan === true) {
        // obj.props.colSpan = 0;
        obj.props.className = 'column-colspan-title-color';
    }
    if (row.stateType === 1) {
        obj.props.className = 'column-error';
    } else if (row.stateType === 2) {
        obj.props.className = 'column-other';
    }
    return obj;
};