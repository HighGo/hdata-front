import React from 'react';

import Header from './Header';

import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import reqwest from 'reqwest';
import '../../css/memoryInfo.css';

export default class MemoryInfo extends React.Component {
    state = {
        momoryInfoList: []
    };
    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getDBMemoryInfoURL,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            success(resp) {
                console.log(resp);
                that.setState({momoryInfoList: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };

    render() {
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_MEMORY_TEXT}/>
                <div className="memory-content">

                    {
                        this.state.momoryInfoList.map(function (item, index) {
                            return (
                                <Panel key={index} title={item.name} dataList={item.dataList} id={item.name}/>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

class Panel extends React.Component {
    render() {
        return (
            <div className="memory-panel">
                <Pie id={this.props.id} dataList={this.props.dataList}/>
                <div className="title">{this.props.title}</div>
            </div>
        )
    }
}

var echarts = require('echarts/lib/echarts');
require('echarts/lib/chart/pie');
require('echarts/lib/component/tooltip');

class Pie extends React.Component {
    componentDidMount = () => {
        let myChart = echarts.init(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.setOption(this.getOption(this.props.dataList));
            window.addEventListener("resize", this.handler, false);
        }
    };
    handler = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.setOption(myChart.getOption);
            myChart.resize();
        }
    };

    componentWillUnmount() {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.dispose();
        }
        window.removeEventListener("resize", this.handler, false);
    };

    getOption = (pieDataList) => {
        console.log(pieDataList)
        return {
            tooltip: {
                trigger: 'item',
                formatter: "{b}<br/> {d}%"
            },
            series: {
                name: '访问来源',
                type: 'pie',
                radius: '55%',
                clockwise: false,
                minAngle: 12,
                center: ['50%', '50%'],
                data: pieDataList,
                itemStyle: {
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label: {
                    normal: {
                        formatter: "{b}\n\n{d}%"
                    }
                }
            }
        }
    };

    render() {
        return (
            <div id={this.props.id} style={{width: '100%', height: '50vh', backgroundColor: 'white'}}/>
        )
    }
}
