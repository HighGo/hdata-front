import React from 'react';
import {message} from 'antd';

import '../../css/lock.css';
import '../../css/SQLAnalyze.css';

import Header from './Header';
import GetInstanceNames from './GetInstanceNames';
import GetSnapIntervalTime from './GetSnapIntervalTime';
import GetInstanceDimension from './GetInstanceDimension';

import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import reqwest from 'reqwest';

export default class SQLAnalyze extends React.Component {
    state = {
        radio: '',
        datePickerDisabled: true,//datePicker is disabled
        datePickerDay: '',   //datePicker day value
        datePickerHourList: [],   //datePicker Hour value
        datePickerItem: '',  //select snapIntervalTime Item
        datePickerEndTime: '',   //show end time
        instanceNames: [],
        snapIntervalTime: [],
        dimension: 'ELAPSED_TIME_DELTA',
        SQLInfo: [],
        SQLPlanStatistics: [],
        SQLID: ''//tr background-color
    };
    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getInstanceNamesURL,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            success(resp) {
                // console.log(resp);
                that.setState({instanceNames: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    handleQuery = () => {
        //onclick query
        console.log('query');
        // console.log(this.state.radio);

        if (this.state.radio === '') {
            message.warning('请选择实例....');
            return false;
        } else if (this.state.datePickerItem === '') {
            message.warning('请选择时间....');
            return false;
        }

        let that = this;
        reqwest({
            url: Properties.getDBSQLInfoURL,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            data: {
                instanceNumber: this.state.radio,
                columnName: this.state.dimension,
                snapId: this.state.datePickerItem.snapId
            },
            success(resp) {
                console.log(resp);
                that.setState({SQLInfo: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    handleInstanceNamesChange = (e) => {
        //onclick instance name
        console.log(`radio checked:${e.target.value}`);
        this.setState(
            {
                radio: e.target.value,
                datePickerDisabled: false
            }
        );

        let that = this;
        reqwest({
            url: Properties.getSnapIntervalTimeURL + e.target.value,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            success(resp) {
                console.log(resp);
                that.setState({snapIntervalTime: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    handleDatePickerChange = (e) => {
        //datePicker change
        const day = e.format('YYYY-MM-DD');
        console.log(day)
        const hours = [];
        for (let i = 0; i < this.state.snapIntervalTime.length; i++) {
            if (this.state.snapIntervalTime[i].beginTime.indexOf(day) > -1)
                hours.push({key: i, name: this.state.snapIntervalTime[i].beginTime})
        }
        console.log(hours)
        this.setState({datePickerDay: day, datePickerHourList: hours});
    };
    handleSnapIntervalTimeChange = (e) => {
        //onclick select datePicker HourList
        this.setState({datePickerItem: this.state.snapIntervalTime[e]})
    };
    handleDimensionChange = (e) => {
        //onclick dimension
        console.log(`radio checked:${e.target.value}`);
        this.setState({dimension: e.target.value});
    };
    handleOnClickQuerySQLId = (value, e) => {
        //onclick sql id
        let that = this;
        reqwest({
            url: Properties.getDBSQLPlanURL,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            data: {
                instanceNumber: this.state.radio,
                sqlId: value,
                snapId: this.state.datePickerItem.snapId,
                endTime: this.state.datePickerItem.endTimeValue
            },
            success(resp) {
                console.log(resp);
                // resp.dataList.push(resp.dataList[0]);
                that.setState({SQLPlanStatistics: resp.dataList, SQLID: value})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    handExportExcel = () => {
        console.log(Properties.getSQLPLanExcelURL + '?instanceNumber=' + this.state.radio + '&sqlId=' + this.state.SQLID
            + '&snapId=' + this.state.datePickerItem.snapId + '&endTime=' + this.state.datePickerItem.endTimeValue)
        //export excel
        window.location.href = Properties.getSQLPLanExcelURL + '?instanceNumber=' + this.state.radio + '&sqlId=' + this.state.SQLID
            + '&snapId=' + this.state.datePickerItem.snapId + '&endTime=' + this.state.datePickerItem.endTimeValue;
    };

    render() {
        let that = this;
        let dimensionOnLine = true;
        let pcWidth = document.body.clientWidth;
        if (pcWidth < 1450) {
            dimensionOnLine = false;
        }

        // const calc = [{"id": '1', "name": 'RAC1'}, {"id": '2', "name": 'RAC2'}];

        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_SQL_TEXT}/>
                <div style={{width: '97%', margin: '10px auto'}}>
                    <table className="HistoryFault">
                        <tbody>
                        <GetInstanceNames onChange={this.handleInstanceNamesChange}
                                          instanceNames={this.state.instanceNames}/>
                        <GetSnapIntervalTime onChange={this.handleSnapIntervalTimeChange}
                                             datePickerChange={this.handleDatePickerChange}
                                             datePickerDisabled={this.state.datePickerDisabled}
                                             selectDatePickerHourList={this.state.datePickerHourList}
                                             snapIntervalTime={this.state.snapIntervalTime}
                                             datePickerItem={this.state.datePickerItem}
                        />
                        <GetInstanceDimension dimensionOnLine={dimensionOnLine}
                                              onClickDimensionChange={this.handleDimensionChange}
                                              onQuery={this.handleQuery} dimension={this.state.dimension}/>
                        <tr style={{height: '1px'}}>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                    <div className="sqlanalyze-content">
                        <div style={{width: '25%'}}>
                            <table className="sqlid">
                                <tbody>
                                <tr className="title">
                                    <td>序号</td>
                                    <td>SQL_ID</td>
                                </tr>
                                {
                                    this.state.SQLInfo.map(function (item, index) {
                                        if (that.state.SQLID === item) {
                                            return (
                                                <tr key={index} onClick={that.handleOnClickQuerySQLId.bind(this, item)}
                                                    style={{backgroundColor: '#eee'}}>
                                                    <td>{index + 1}</td>
                                                    <td>{item}</td>
                                                </tr>
                                            )
                                        }
                                        return (
                                            <tr key={index} onClick={that.handleOnClickQuerySQLId.bind(this, item)}>
                                                <td>{index + 1}</td>
                                                <td>{item}</td>
                                            </tr>
                                        )
                                    })
                                }
                                </tbody>
                            </table>
                        </div>
                        <div className="plan">
                            {

                                this.state.SQLPlanStatistics.map(function (item, index) {
                                    return (
                                        <div key={index} className="item">
                                            <Plan item={item} title="执行计划" plan="sqlPlan"
                                                  exportExcel={that.handExportExcel}/>
                                            <Plan item={item} title="Sql Statistics" plan="sqlStatistics"/>
                                        </div>

                                    )
                                })
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const Plan = (props) => {
    // console.log(props.item)

    //some column is may be not visible
    let partitionStartVisable = props.item.sqlPlan.partitionStartVisable ? 'table-cell' : 'none';
    let partitionStopVisable = props.item.sqlPlan.partitionStopVisable ? 'table-cell' : 'none';
    let partitionIdVisable = props.item.sqlPlan.partitionIdVisable ? 'table-cell' : 'none';
    let accessPredicatesVisable = props.item.sqlPlan.accessPredicatesVisable ? 'table-cell' : 'none';
    let filterPredicatesVisable = props.item.sqlPlan.filterPredicatesVisable ? 'table-cell' : 'none';

    const list = {th: []};
    if (props.plan === 'sqlPlan') {
        const th = [{title: 'Id'}, {title: 'Operation'}, {title: 'Owner'}, {title: 'Name'},
            {title: 'Rows'}, {title: 'Bytes'}, {title: 'Cost(%CPU)'}, {title: 'Time'},
            {title: 'Start', display: partitionStartVisable}, {title: 'Stop', display: partitionStopVisable},
            {title: 'partition', display: partitionIdVisable}, {title: 'access', display: accessPredicatesVisable},
            {title: 'Ifilterd', display: filterPredicatesVisable}];
        list.th = th;
    } else {
        const th = [{title: 'Stat Name', align: 'center'}, {
            title: 'Statement Total',
            align: 'center'
        }, {title: 'Per Execution', align: 'center'}];
        list.th = th;
    }
    return (
        <div>
            {/*div header*/}
            <div className="title">
                {props.title}——{props.item.planHashValue}
                {
                    props.plan === 'sqlPlan' ?

                        <button style={{float: 'right', margin: '6px 2% 0px 0px'}} className="buttonItem"
                                onClick={props.exportExcel}>
                            导出
                        </button>
                        : ''
                }
            </div>

            {/*div centent*/}
            <div style={{overflow: 'auto'}}>
                <table className="executable">
                    <tbody>
                    <tr>
                        {
                            list.th.map(function (item, index) {
                                return (
                                    <td key={index}
                                        style={{
                                            display: item.display === undefined ? 'table-cell' : item.display,
                                            textAlign: item.align === undefined ? 'center' : item.align
                                        }}>
                                        <pre>{item.title}</pre>
                                    </td>
                                )
                            })
                        }
                    </tr>
                    {
                        props.plan === 'sqlPlan' ?
                            props.item.sqlPlan.sqlPlans.map(function (item, index) {
                                return (
                                    <tr key={index}>
                                        <td>{item.id}</td>
                                        <td style={{textAlign: 'left'}}>
                                            <pre>{item.operation}</pre>
                                        </td>
                                        <td>{item.owner}</td>
                                        <td>{item.name}</td>
                                        <td>{item.rows}</td>
                                        <td>{item.bytes}</td>
                                        <td>{item.cost}</td>
                                        <td>{item.time}</td>
                                        <td style={{display: partitionStartVisable}}>{item.partitionStart}</td>
                                        <td style={{display: partitionStopVisable}}>{item.partitionStop}</td>
                                        <td style={{display: partitionIdVisable}}>{item.partitionId}</td>
                                        <td style={{display: accessPredicatesVisable}}>{item.accessPredicates}</td>
                                        <td style={{display: filterPredicatesVisable}}>{item.filterPredicates}</td>
                                    </tr>
                                )
                            }) :
                            props.item.sqlStatistics.map(function (item, index) {
                                return (
                                    <tr key={index}>
                                        <td>{item.statName}</td>
                                        <td>{item.statementTotal}</td>
                                        <td>{item.perExecution}</td>
                                    </tr>
                                )
                            })
                    }
                    </tbody>
                </table>
            </div>
        </div>
    )
};


