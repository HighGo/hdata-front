import React from 'react';
import {Radio} from 'antd';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

export default class GetInstanceNames extends React.Component{
    render(){
        console.log(this.props.instanceNames)
        return(
            <tr>
                <td className="title">选择实例</td>
                <td>
                    <RadioGroup onChange={this.props.onChange}>
                        {
                            this.props.instanceNames.map(function (item, index) {
                                return (
                                    <RadioButton key={index} value={item.instanceId}>
                                        {item.instanceName}
                                    </RadioButton>
                                )
                            })
                        }
                    </RadioGroup>
                </td>
            </tr>
        )
    }
}