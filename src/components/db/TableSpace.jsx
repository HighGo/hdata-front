import React from 'react';
import '../../css/lock.css';


import {Tables} from "./Tables"
import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import Header from './Header';

export default class TableSpace extends React.Component {
    render() {
        const columns = [{
            title: '表空间名称',
            dataIndex: 'name',
            render: renderContent,
        }, {
            title: '总大小(GB不包括自动扩展)',
            dataIndex: 'sizeTotalWithoutExtend_',
            render: renderContent,
        },
            {
                title: '总大小(GB 包括自动扩展)',
                dataIndex: 'sizeTotal',
                render: renderContent,
            }, {
                title: '空闲空间(GB 包括自动扩展)',
                dataIndex: 'freeSize',
                render: renderContent,
            }, {
                title: '已使用空间(GB)',
                dataIndex: 'usedSize',
                render: renderContent,
            }, {
                title: '使用率(% 考虑自动扩展)',
                dataIndex: 'rate',
                render: renderContent,
            }];
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_TABLESPACE_TEXT}/>
                <div style={{width: '100%', margin: '0 auto'}}>
                    <Tables id="asm" columns={columns} url={Properties.getTableSpaceRateURL}/>
                </div>
            </div>
        );
    }
}
const renderContent = (value, row, index) => {
    const obj = {
        children: value,
        props: {},
    };
    if (row.stateType === 1) {
        obj.props.className = 'column-error';
    }
    return obj;
};

