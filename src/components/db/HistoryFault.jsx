import React from 'react';

import Header from './Header';
import GetInstanceNames from './GetInstanceNames';
import GetSnapIntervalTime from './GetSnapIntervalTime';

import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import reqwest from 'reqwest';
import '../../css/dbHistroyFault.css';
import {message} from 'antd';

export default class HistoryFault extends React.Component {
    state = {
        radio: '',
        datePickerDisabled: true,//datePicker is disabled
        datePickerDay: '',   //datePicker day value
        datePickerHourList: [],   //datePicker Hour value
        datePickerItem: '',  //select snapIntervalTime Item
        datePickerEndTime: '',   //show end time
        instanceNames: [],
        snapIntervalTime: [],
        pieDataList: []

    };
    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getInstanceNamesURL,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            success(resp) {
                // console.log(resp);
                that.setState({instanceNames: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    handleQuery = () => {
        console.log('query');
        // console.log(this.state.radio);

        if (this.state.radio === '') {
            message.warning('请选择实例....');
            return false;
        } else if (this.state.datePickerItem === '') {
            message.warning('请选择时间....');
            return false;
        }

        let that = this;
        reqwest({
            url: Properties.getHistoricalFailureAnalysisURL,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            data: {instanceNumber: this.state.radio, snapId: this.state.datePickerItem.snapId},
            success(resp) {
                console.log(resp);
                that.setState({pieDataList: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })

    };
    handleInstanceNamesChange = (e) => {
        console.log(`radio checked:${e.target.value}`);
        this.setState(
            {
                radio: e.target.value,
                datePickerDisabled: false
            }
        );

        let that = this;
        reqwest({
            url: Properties.getSnapIntervalTimeURL + e.target.value,
            method: 'get',
            type: 'json',
            contentType: 'application/json',
            success(resp) {
                // console.log(resp);
                that.setState({snapIntervalTime: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    handleDatePickerChange = (e) => {
        //datePicker change
        const day = e.format('YYYY-MM-DD');
        console.log(day)
        const hours = [];
        for (let i = 0; i < this.state.snapIntervalTime.length; i++) {
            if (this.state.snapIntervalTime[i].beginTime.indexOf(day) > -1)
                hours.push({key: i, name: this.state.snapIntervalTime[i].beginTime})
        }
        console.log(hours)
        this.setState({datePickerDay: day, datePickerHourList: hours});
    };
    handleSnapIntervalTimeChange = (e) => {
        //onclick select datePicker HourList
        this.setState({datePickerItem: this.state.snapIntervalTime[e]})
    };

    render() {
        // const calc = [{"id": 'a', "name": 'RAC1'}, {"id": 'b', "name": 'RAC2'}];
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_HISTORY_TEXT}/>
                <div style={{width: '97%', margin: '10px auto'}}>
                    <table className="HistoryFault">
                        <tbody>
                        <GetInstanceNames onChange={this.handleInstanceNamesChange}
                                          instanceNames={this.state.instanceNames}/>
                        <GetSnapIntervalTime onChange={this.handleSnapIntervalTimeChange}
                                             datePickerChange={this.handleDatePickerChange}
                                             datePickerDisabled={this.state.datePickerDisabled}
                                             selectDatePickerHourList={this.state.datePickerHourList}
                                             snapIntervalTime={this.state.snapIntervalTime}
                                             datePickerItem={this.state.datePickerItem}
                        >
                            <span onClick={this.handleQuery}>查询</span>
                        </GetSnapIntervalTime>
                        </tbody>
                    </table>

                    <div style={{
                        margin: '10px auto',
                        backgroundColor: 'white',
                        height: 'auto',
                        border: '1px solid #e9e9e9'
                    }}>
                        <A id="historyFault" pieDataList={this.state.pieDataList}/>
                    </div>

                </div>
            </div>
        )
    }
}


// 引入 ECharts 主模块
var echarts = require('echarts/lib/echarts');
// 引入柱状图
require('echarts/lib/chart/pie');
// 引入提示框和标题组件
require('echarts/lib/component/tooltip');
require('echarts/lib/component/title');

class A extends React.Component {
    state = {
        msg: '',
        msgDisplay: '',
        pieDataList: []
    };
    componentDidMount = () => {

    };
    shouldComponentUpdate = (a, b) => {

        if (a.pieDataList === this.state.pieDataList) {
            return false;
        }
        return true;
    }

    componentWillReceiveProps(nextProps) {//will receive
        this.setState({pieDataList: nextProps.pieDataList, msgDisplay: 'none'});

        if (nextProps.pieDataList.length > 0) {
            let temp = echarts.getInstanceByDom(document.getElementById(this.props.id));
            let myChart = temp === undefined ? echarts.init(document.getElementById(this.props.id)) : temp;
            myChart.setOption(this.getOption(nextProps.pieDataList));

            window.addEventListener("resize", this.handler, false);
        } else {
            let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
            if (myChart !== undefined) {
                myChart.dispose();
            }
            window.removeEventListener("resize", this.handler, false);
            this.setState({msg: '所选时间段内没有阻塞情况发生', msgDisplay: 'block'});
        }
    };

    componentWillUnmount() {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.dispose();
        }
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.setOption(myChart.getOption);
            myChart.resize();
        }
    };
    getOption = (pieDataList) => {

        return {
            title: {
                text: 'enq:TX - row lock contention',
                top: 20,
                left: 'center'
            },
            tooltip: {
                trigger: 'item',
                formatter: "{a} <br/>{b} : {c} ({d}%)"
            },
            series: [
                {
                    name: '故障分析',
                    type: 'pie',
                    radius: '65%',
                    center: ['50%', '50%'],
                    selectedMode: 'single',
                    // data:[
                    //     {
                    //         value: 535,
                    //         name: 'db file sequential red',
                    //         ss: [{name: 'blocker', value: 1.1357}, {name: 'blocker', value: 1.2178}]
                    //     },
                    //     {value: 510, name: 'gc cr grant 2-way', ss: []},
                    //     {value: 634, name: 'gc buffer busy', ss: []},
                    //     {value: 735, name: 'db buffer grant', ss: []}
                    // ],
                    data: pieDataList,
                    itemStyle: {
                        // normal: {
                        //     label: {
                        //         show: true,
                        position: 'inside',
                        // formatter: '{b} : {c} \n ({d}%)',
                        // color: 'black'
                        // },
                        // labelLine: {show: true}
                        // },
                        emphasis: {
                            shadowBlur: 10,
                            shadowOffsetX: 0,
                            shadowColor: 'rgba(0, 0, 0, 0.5)'
                        }
                    },
                    labelLine: {
                        normal: {
                            length: 24,
                            length2: 24
                        }
                    },
                    label: {
                        normal: {
                            // formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b}：}{c}  {per|{d}%}  ',
                            formatter(param) {
                                // console.log(param);
                                let list = param.data.blockerTOs;
                                let blockerList = '';

                                if (list !== undefined && list.length > 0) {
                                    blockerList = '\n{hr|}\n';
                                    for (let i = 0; i < list.length; i++) {
                                        blockerList += '{blocker|blocker：' + list[i].blocker + '：' + list[i].blockedCount + '}';
                                        if (i !== list.length - 1) {
                                            blockerList += '\n';
                                        }
                                    }

                                }
                                return '{name|' + param.name + '：' + param.value + '}    {per|' + param.percent + '%}' + blockerList;
                            },
                            backgroundColor: '#eee',
                            borderColor: '#aaa',
                            borderWidth: 1,
                            borderRadius: 4,
                            rich: {
                                hr: {
                                    borderColor: '#aaa',
                                    width: '100%',
                                    borderWidth: 0.5,
                                    height: 0
                                },
                                name: {
                                    padding: [0, 0, 0, 10],
                                    fontSize: 16,
                                    lineHeight: 33
                                },
                                per: {
                                    color: '#eee',
                                    backgroundColor: '#334455',
                                    padding: [10, 4],
                                    borderRadius: 2
                                },
                                blocker: {
                                    fontSize: 14,
                                    color: 'black',
                                    lineHeight: 18,
                                    padding: [0, 0, 0, 8]
                                }
                            }
                        }
                    },
                    color: ['#8AC44B', '#2B99F0', '#F8873D', '#FBA71A']
                }
            ]
        };
    };

    render() {
        // console.log('render')
        return (
            <div>
                <div style={{
                    marginTop:'15px',
                    textAlign: 'center',
                    fontSize: '16px',
                    display: this.state.msgDisplay
                }}>{this.state.msg}</div>
                <div style={{width: '100%', height: '60vh'}} id={this.props.id}/>
            </div>
        )
    }
}
