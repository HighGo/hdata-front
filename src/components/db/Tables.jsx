import React from 'react';
import {Table} from 'antd';
import reqwest from 'reqwest';


export class Tables extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            // url: this.props.data.tableUrl,
            // params: this.props.data,
            // tableData: null,
            data: [],
            // pagination: {pageSize: 10, total: 0},
            loading: false,
        };
    };

    componentDidMount = () => {//did mount
        let url = this.props.url;
        this.fetch(url);

        this.timerID = setInterval(() => this.fetch(url), 60000);
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    };

    fetch = (url) => {
        let that = this;
        reqwest({
            url: url
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp) {
                console.log(resp);
                that.setState({data: resp.dataList})
            }, error(resp) {
                console.log(resp)
            }
        })
    };

    render() {
        // const columns = [{
        //     title: 'Name',
        //     dataIndex: 'name',
        //     render: (text, row, index) => {
        //         const obj = {
        //             children: text,
        //             props: {},
        //         };
        //
        //         if (row.colSpan === true) {
        //             obj.props.children = <div>{text}</div>;
        //             // obj.props.colSpan = 4;
        //             obj.props.className = 'column-colspan-title';
        //             return obj;
        //         }
        //         if (row.rowSpan !== 1) {
        //             obj.props.rowSpan = row.rowSpan;
        //         }
        //
        //         return obj;
        //     },
        // }, {
        //     title: 'TARGET',
        //     dataIndex: 'target',
        //     render: renderContent,
        // },
        //     {
        //         title: 'STATE',
        //         dataIndex: 'state',
        //         render: renderContent,
        //     }, {
        //         title: 'SERVER',
        //         dataIndex: 'server',
        //         render: renderContent,
        //     }, {
        //         title: 'STATE_DETAILS',
        //         dataIndex: 'state_details',
        //         render: renderContent,
        //     }];

        // const data = [{
        //     key: '0',
        //     name: 'Jake White',
        //     age: '',
        //     tel: '',
        //     phone: '',
        //     address: '',
        //     isColSpan: 'yes',
        //     rowSpan: 1
        //
        // }, {
        //     key: '1',
        //     name: 'John Brown',
        //     age: 32,
        //     tel: '0571-22098909',
        //     address: 'New York No. 1 Lake Park',
        //     type: '1',
        //     rowSpan: 2
        // }, {
        //     key: '2',
        //     name: 'Jim Green',
        //     tel: '0571-22098333',
        //     age: 42,
        //     address: 'London No. 1 Lake Park',
        //     type: '2',
        //     rowSpan: 0
        // }, {
        //     key: '3',
        //     name: 'Joe Black',
        //     age: 32,
        //     tel: '0575-22098909',
        //     address: 'Sidney No. 1 Lake Park',
        //     type: '1',
        //     rowSpan: 1
        // }, {
        //     key: '4',
        //     name: 'Jim Red',
        //     age: 18,
        //     tel: '0575-22098909',
        //     address: 'London No. 2 Lake Park',
        //     type: '3',
        //     rowSpan: 1
        // }];
        return (
            <div className="cluster">
                <Table
                    // rowKey={record => record.id}
                    columns={this.props.columns}
                    dataSource={this.state.data}
                    pagination={false}
                    size="small"
                    // scroll={{x: true, y: 300}}
                    bordered={true}
                    showHeader
                    // loading={this.state.loading}
                    // onChange={this.handleTableChange}
                />
            </div>
        )
    }
}

