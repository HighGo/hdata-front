import React from 'react';

import '../../css/calc.css'
import history from '../../img/icon-history.png';
import monitor from '../../img/icon-monitor.png';
import DeviceName from '../calc/DeviceName';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import * as constants from '../constants/Constants';
import SessionService from './SessionService';

export default class DbIndex extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            calcList: []
        }
    };

    componentDidMount = () => {
        this.fetch({
            url: Properties.getSystemAvailabilityURL,
            method: 'get'
        });
    };
    fetch = (params = {}) => {
        let that = this;
        //device list request
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
            , success(resp) {
                // console.log(resp);
                that.setState({calcList: resp.data.data});
            }, error(error) {
                console.log(params.url);
                console.log(error);
            }

        });
    };
    handTabClick = (value, e) => {
        console.log(value);
        this.setState({tab: value})
    };

    render() {
        if (this.state.calcList.length === 0) {
            return null;
        }
        console.log('Dbindex...view...');

        return (
            <div className="calc">
                {
                    this.state.calcList.map(function (item, index) {

                        if (item.type === 'storagenode') {
                            return null;
                        }
                        return (
                            <div key={index}>
                                <div className="header">
                                    <DeviceName id={item.id} />
                                    <div style={{marginLeft: '2%'}} className="hdata-tabs-tab hdata-tabs-tab-active"
                                        // className={this.state.tab === 1 ? 'hdata-tabs-tab hdata-tabs-tab-active' : 'hdata-tabs-tab'}
                                        // onClick={this.handTabClick.bind(this, 1)}
                                    >
                                        <img alt="" src={monitor} className=""/>实时监控
                                    </div>
                                    <div className="hdata-tabs-tab"
                                        // className={this.state.tab === 2 ? 'hdata-tabs-tab hdata-tabs-tab-active' : 'hdata-tabs-tab'}
                                        // onClick={this.handTabClick.bind(this, 2)}
                                    >
                                        <span><img alt="" src={history}/>历史数据</span>
                                    </div>
                                </div>
                                <div className="flex-item" style={{width:'98%',margin:'15px auto',backgroundColor:'white',border:'1px solid #eee'}}>
                                    <div className="panelTitle" style={{height:'36px',lineHeight:'36px',color:'black',fongtSize:'15px'}}>{constants.DBINDEX_SESSION_TEXT}</div>
                                    <SessionService id={'session' + index} height = '380px' deviceId={item.id} />
                                </div>
                            </div>
                        )
                    })
                }
                {/*{this.state.tab === 1 ? <Monitor id={this.state.id}/> : <div> </div>}*/}
            </div>
        )
    }
}

