import React from 'react';
import '../../css/lock.css';

import {Tables} from "./Tables";
import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import Header from './Header';

export default class DBRMAN extends React.Component {
    render() {
        const columns = [{
            title: '会话ID',
            dataIndex: 'sessionId',
            render: renderContent,
        }, {
            title: '输出设备类型',
            dataIndex: 'outputDeviceType',
            render: renderContent,
        },
            {
                title: '备份类型',
                dataIndex: 'inputType',
                render: renderContent,
            }, {
                title: '状态',
                dataIndex: 'status',
                render: renderContent,
            }, {
                title: '耗时(小时)',
                dataIndex: 'elapsedHours',
                render: renderContent,
            }, {
                title: '备份开始时间',
                dataIndex: 'startTime',
                render: renderContent,
            }, {
                title: '备份大小',
                dataIndex: 'outSize',
                render: renderContent,
            }, {
                title: '每秒输出',
                dataIndex: 'outputBytesPerSecDisplay',
                render: renderContent,
            }, {
                title: '每秒输入',
                dataIndex: 'inputBytesPerSecDisplay',
                render: renderContent,
            }];
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_RMAN_TEXT}/>
                <div style={{width: '100%', margin: '0 auto'}}>
                    <Tables id="asm" columns={columns} url={Properties.getRMANBackupInfoURL}/>
                </div>
            </div>
        );
    }
}

const renderContent = (value, row, index) => {
    const obj = {
        children: value,
        props: {},
    };
    if (row.stateType === 1) {
        obj.props.className = 'column-error';
    }
    return obj;
};