import React from 'react';
import '../../css/lock.css';

import reqwest from 'reqwest';
import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import Header from './Header';

export default class AsmStatus extends React.Component {
    render() {
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_ASMSTATUS_TEXT}/>
                <div style={{width: '97%', backgroundColor: 'white', margin: '1% auto', border: '1px solid #e9e9e9'}}>
                    <ASM_Column/>
                </div>
            </div>
        );
    }
}

class ASM_Column extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    };

    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getAsmDiskStatusURL
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp) {
                console.log(resp);
                that.setState({data: resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };

    render() {
        // const list = [
        //     {'id': 1, 'status': 1, 'name': '存储节点1'},
        //     {'id': 2, 'status': 1, 'name': '存储节点2'},
        //     {'id': 3, 'status': 2, 'name': '存储节点3'}
        // ];
        return (
            <div className="asm">
                <div className="legend-line">
                    <div className="legend">
                        <Legend color={'#8AC44B '} name={constants.DBINDEX_ASMSTATUS_LEGEND_TEXT1}/>
                        <Legend color={'#FE6512'} name={constants.DBINDEX_ASMSTATUS_LEGEND_TEXT2}/>
                    </div>
                </div>
                <div className="details">
                    {
                        this.state.data.map(function (item, index) {
                            let img = Img2;//init unnormal
                            if (item.status === constants.DBINDEX_ASMSTATUS_STATUS_TEXT) {
                                img = Img1;
                            }
                            let name = constants.DBINDEX_ASMSTATUS_DATA_VALUE1_TEXT;
                            if (item.name === constants.DBINDEX_ASMSTATUS_DATA_VALUE2) {
                                name = constants.DBINDEX_ASMSTATUS_DATA_VALUE2_TEXT;
                            } else if (item.name === constants.DBINDEX_ASMSTATUS_DATA_VALUE3) {
                                name = constants.DBINDEX_ASMSTATUS_DATA_VALUE3_TEXT;
                            }
                            return (
                                <div className="details-item" key={item.id}>
                                    <img src={img}/>
                                    <br/><br/>
                                    <div>{name}</div>
                                </div>
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

const Legend = (props) => {
    return (
        <div className="legend-item">
            <div style={{backgroundColor: props.color, width: '25px', height: '10px', margin: '4px 6px'}}/>
            <div>{props.name}</div>
        </div>
    )
};
import Img1 from '../../img/storage-green.png';
import Img2 from '../../img/storage-orange.png';