import React from 'react';
import echarts from 'echarts/lib/echarts';
import ColumnMap from '../container/ColumnMap';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';

export default class SessionService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            monitorData: []
        }
    }

    componentDidMount = () => {
        console.log('SessionService...componentDidMount...' + this.props.deviceId);
        this.fetch({
            url: Properties.getVSessionSnapURL + this.props.deviceId,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: Properties.getVSessionSnapURL + this.props.deviceId,
                method: 'get'
            }),
            Properties.getAllCpuStatusURL_TIME
        );
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    };

    fetch = (params = {}) => {
        let that = this;
        // console.log(params.url)
        //device list request
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
            , success(resp) {
                // console.log(resp);
                if (document.getElementById(that.props.id) == null) {
                    that.setState({monitorData: resp.data});
                } else {
                    that.dynamic(resp.data)
                }
            }, error(error) {
                console.log(params.url);
                console.log(error);
            }
        });
    };
    dynamic = (resp) => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let option = myChart.getOption();

        // console.log(option);
        // console.log(resp);
        let tempText = [];
        for (let i in resp.loadLineList) {

            tempText = resp.loadLineList[i].text;
            // if (i === '4') {
            //     tempText = [{value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}];
            // } else if (i === '6') {
            //     tempText = [{value: 22}, {value: 44}, {value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}];
            // } else if (i === '7') {
            //     tempText = [{value: 22}, {value: 44}, {value: 12}, {value: 13}, {value: 12}, {value: 13}, {value: 12}, {value: 13}];
            // }
            option.series[i].data = tempText;
        }
        option.xAxis[0].data = resp.xdata;

        myChart.setOption(option);
    };

    render() {
        if (this.state.monitorData.length === 0) {
            return null;
        }
        // console.log('SessionService.....render...');
        // console.log(this.state.monitorData)
        return (
            <ColumnMap id={this.props.id} height={this.props.height} monitorData={this.state.monitorData}/>
        )
    }
}