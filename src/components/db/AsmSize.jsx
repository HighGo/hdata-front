import React from 'react';
import '../../css/lock.css';

import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';

import reqwest from 'reqwest';
import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import Header from './Header';

export default class AsmSize extends React.Component {
    render() {
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_ASMSIZE_TEXT}/>
                <div style={{width: '97%', backgroundColor: 'white', margin: '1% auto', border: '1px solid #e9e9e9'}}>
                    <ASM_Column id="asm"/>
                </div>
            </div>
        );
    }
}

class ASM_Column extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }
    }

    componentDidMount = () => {
        let that = this;
        reqwest({
            url: Properties.getAsmDiskgroupInfoURL
            , method: 'get'
            , contentType: 'application/json'
            , type: 'json'
            , success(resp) {
                // console.log(resp);
                that.setState({data: resp.data})

                let myChart = echarts.init(document.getElementById(that.props.id));
                let options = that.setOption(resp.data);
                myChart.setOption(options);
                window.addEventListener("resize", that.handler, false);

            }, error(resp) {
                console.log(resp);
            }
        })


    };

    componentWillUnmount() {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.dispose();
        }
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart !== undefined) {
            myChart.setOption(myChart.getOption);
            myChart.resize();
        }
    };
    setOption = (data) => {
        // console.log(data)
        let legend = [];
        let series = [];
        let name;
        let color;

        for (let i = 0; i < data.series.length; i++) {
            if (data.legend[i] === constants.DBINDEX_ASMSIZE_LEGEND) {
                name = constants.DBINDEX_ASMSIZE_LEGEND_NOTUSED_TEXT;
                color = '#444';
            } else {
                name = constants.DBINDEX_ASMSIZE_LEGEND_USABLE_TEXT;
                color = 'white';
            }
            legend.push({name: name, icon: 'rect'});

            series.push({
                name: name,
                type: 'bar',
                stack: 'bar',
                silent: true,
                label: {
                    normal: {
                        show: true,
                        formatter(params) {
                            return params.data.name;
                        }
                    }
                },
                itemStyle: {
                    normal: {
                        label: {textStyle: {color: color}}
                    }
                },
                data: data.series[i].data,
                barMinHeight:24
            })
        }

        // console.log(series);

        let xAxis = [];
        let tempXAxis = '';
        for (let i = 0; i < data.xAxis.length; i++) {
            tempXAxis = data.xAxis[i].split(',');
            xAxis.push({value: '\t' + tempXAxis[0] + '\n\n' + tempXAxis[1] + 'G'});
        }


        return {
            tooltip: {
                show: false,
                formatter(param) {
                    console.log(param[0])
                }
            },
            legend: {
                show: true,
                top: '20', // 'center' | 'bottom' | {number}
                right: '30',
                align: 'left',
                orient: 'vertical',//，horizontal|vertical
                backgroundColor: 'white',
                borderColor: '#ddd',
                borderWidth: 1,
                padding: [20, 20, 20, 20],
                itemWidth: 25,
                itemHeight: 10,
                itemGap: 15,
                // data: [{name: '已用空间', icon: 'rect'}, {name: '空闲空间', icon: 'rect'}],
                data: legend,
                selectedMode: false,
                // width:150,
                textStyle:{
                    padding:[0,0,0,10]
                }
            },
            grid: {
                width: '70%',
                height: '80%',
                containLabel: true
            },
            yAxis: {
                type: 'value',
                show: false
            },
            xAxis: {
                type: 'category',
                data: xAxis,
                // data: ['\tOCR\n\n 10G', '\tDATA \n\n 100G', '\tOFA \n\n 100G'],
                axisLabel: {
                    textStyle: {
                        fontWeight: 'bold'
                    }
                },
                axisLine: {
                    show: false
                },
                axisTick: {
                    show: false
                }
            },
            series: series,
            // series: [
            //     {
            //         name: '已用空间',
            //         type: 'bar',
            //         stack: 'bar',
            //         silent: true,
            //         label: {
            //             normal: {
            //                 show: true,
            //                 formatter(params) {
            //                     return params.data.name;
            //                 }
            //             }
            //         },
            //         itemStyle: {
            //             normal: {
            //                 label: {textStyle: {color: '#444'}}
            //             }
            //         },
            //         data: [
            //             {value: 65, name: '65%'},
            //             {value: 120, name: '30%'},
            //             {value: 180, name: '60%'}
            //         ],
            //
            //     },
            //
            //     {
            //         name: '可用空间',
            //         type: 'bar',
            //         stack: 'bar',
            //         barMaxWidth: '40%',
            //         silent: true,
            //         label: {
            //             normal: {
            //                 show: true,
            //                 formatter(params) {
            //                     return params.data.name;
            //                 }
            //             }
            //         },
            //         // itemStyle:{
            //         //     normal: {
            //         //         label: {textStyle:{color:'#444'}}
            //         //     }
            //         // },
            //         data: [
            //             {value: 35, name: '35%'},
            //             {value: 280, name: '70%'},
            //             {value: 120, name: '40%'}
            //         ]
            //
            //     },
            // ],
            color: ['#E4E4E4', '#8AC44B']
        }
    };

    render() {
        return (
            <div id={this.props.id} style={{width: '100%', height: '50vh'}}/>
        )
    }
}