import React from 'react';
import {Radio} from 'antd';
const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

export default class GetInstanceDimension extends React.Component{
    render(){
        const dimension = [{"id": 1, "value": 'ELAPSED_TIME_DELTA', "name": 'Elapsed Time'}, {
            "id": 2,
            "value": 'CPU_TIME_DELTA',
            "name": 'CPU Time'
        },
            {"id": 3, "value": 'IOWAIT_DELTA', "name": 'User I/O Wait Time'}, {
                "id": 4,
                "value": 'BUFFER_GETS_DELTA',
                "name": 'Get'
            },
            {"id": 5, "value": 'DISK_READS_DELTA', "name": 'Read'}, {
                "id": 6,
                "value": 'EXECUTIONS_DELTA',
                "name": 'Excutions'
            },
            {"id": 7, "value": 'PARSE_CALLS_DELTA', "name": 'Parse Calls'}, {
                "id": 8,
                "value": 'SHARABLE_MEM',
                "name": 'Sharable Memory'
            },
            {"id": 9, "value": 'VERSION_COUNT', "name": 'Version Count'}, {
                "id": 10,
                "value": 'CLWAIT_DELTA',
                "name": 'Cluster Wait Time'
            },];
        return(
            <tr>
                <td className="title">选择维度</td>

                <td rowSpan={this.props.dimensionOnLine?'1':'2'}>
                    <div className={this.props.dimensionOnLine?'':'dimensionDivSmall'}>
                        <RadioGroup onChange={this.props.onClickDimensionChange} defaultValue={this.props.dimension}>
                            {
                                dimension.map(function (item, index) {
                                    return (
                                        <RadioButton key={index} value={item.value}>
                                            {item.name}
                                        </RadioButton>
                                    )
                                })
                            }
                            <button className="buttonItem buttonItemSelect" onClick={this.props.onQuery} style={{marginLeft: '10px'}}>查询</button>
                        </RadioGroup>

                    </div>
                </td>
            </tr>
        )
    }
}