import React from 'react';
import '../../css/lock.css';
import SortableTree from 'react-sortable-tree';

import reqwest from 'reqwest';
import * as constants from '../constants/Constants';
import Properties from '../constants/Properties';
import Header from './Header';

export default class Lock extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            treeData: []
        }
    }

    componentDidMount = () => {
        let that = this;
        console.log(Properties.getLockTreeURL)
        reqwest({
            url: Properties.getLockTreeURL
            , method: 'get'
            , type: 'json'
            , contentType: 'application/json'
            , success(resp) {
                console.log(resp);
                that.setState({treeData:resp.dataList})
            }, error(resp) {
                console.log(resp);
            }
        })
    };
    test = (a) => {
        // console.log(a)
    };

    render() {

        // const treeData = [{
        //     title: '1.1903', children: [
        //         {
        //             title: '1.2094',
        //             children: [{title: '1.1111'}],
        //             expanded: true
        //         },
        //         {title: '1.576'}, {title: '2.1716'}, {title: '2.2094'}, {title: '2.6'}
        //     ], expanded: true
        // }];
        let that = this;
        let tempArray = [];
        return (
            <div className="lock">
                <Header title={constants.DB_INDEX_LOCK_TEXT}/>
                <div className="lock-content">

                        {
                            this.state.treeData.map(function (item,index) {
                                // console.log(item)
                                tempArray = [];
                                tempArray.push(item)
                                return (
                                    <div className="column">
                                    <SortableTree
                                        key ={index}
                                        canDrag={false}
                                        treeData={tempArray}
                                        onChange={that.test}
                                    />
                                    </div>
                                )
                            })
                        }


                </div>

            </div>


        );
    }
}