var ip = document.location.hostname;
var TIME_UP = 11000;
const components = {

    cpuInterval: 1000 * 60 * 30,//30minute
    getMenuListURL: 'http://' + ip + ':8080/hdatamonitor/api/getDevicedForNavigation',

    //watch
    getAllCpuStatusURL: 'http://' + ip + ':8080/hdatamonitor/api/getAllCpuStatus',//cpu
    getAllCpuStatusURL_TIME: TIME_UP,
    getLoadAverageURL: 'http://' + ip + ':8080/hdatamonitor/api/getLoadAverage',//load
    getLoadAverageURL_TIME: TIME_UP,
    getSystemAvailabilityURL: 'http://' + ip + ':8080/hdatamonitor/api/getSystemAvailability',//system
    getSystemAvailabilityURL_TIME: TIME_UP,
    getSystemAvailabilityURL_RUNNING: 'running',
    getALlIOLoadURL: 'http://' + ip + ':8080/hdatamonitor/api/getALlIOLoad',//io
    getALlIOLoadURL_TIME: TIME_UP,
    getAllHCAURL: 'http://' + ip + ':8080/hdatamonitor/api/getAllHCA',//network
    getDatabaseSysEventURL: 'http://' + ip + ':8080/hdatamonitor/api/getDatabaseSysEvent/',//top5

    getMonitorLoginfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getMonitorLoginfo',//log
    getMonitorLoginfoURL_TIME: TIME_UP,
    deleteMonitorLogURL: 'http://' + ip + ':8080/hdatamonitor/api/deleteMonitorLog',//delete log


    //calc
    getLoadAverageByDeviceIdURL: 'http://' + ip + ':8080/hdatamonitor/api/getLoadAverageByDeviceId/',//load
    getLoadAverageByDeviceIdURL_TIME: TIME_UP,
    getDeviceStatusURL: 'http://' + ip + ':8080/hdatamonitor/api/getDeviceStatus/',//device
    getCpuStatusURL: 'http://' + ip + ':8080/hdatamonitor/api/getCpuStatus/',//cpu
    getCpuStatusURL_TIME: TIME_UP,
    getMemoryStatusURL: 'http://' + ip + ':8080/hdatamonitor/api/getMemoryStatus/',//memory
    getIOLoadURL: 'http://' + ip + ':8080/hdatamonitor/api/getIOLoad/',//io
    getCpuTopPidURL: 'http://' + ip + ':8080/hdatamonitor/api/getCpuTopPid/',//cpu top
    getCpuTopPidURL_TIME: TIME_UP,
    getMemoryTopPidURL: 'http://' + ip + ':8080/hdatamonitor/api/getMemoryTopPid/',//memory top
    getIoTopPidURL: 'http://' + ip + ':8080/hdatamonitor/api/getIoTopPid/',//io top
    getNicPublicURL: 'http://' + ip + ':8080/hdatamonitor/api/getNicPublic/',//public
    getNicPublicURL_TIME: TIME_UP,
    getNicPrivateURL: 'http://' + ip + ':8080/hdatamonitor/api/getNicPrivate/',//Private
    getHCAURL: 'http://' + ip + ':8080/hdatamonitor/api/getHCA/',//hca
    getMountPointUseURL: 'http://' + ip + ':8080/hdatamonitor/api/getMountPointUse/',//MountPointUse
    getMountPointUseURL_TIME: TIME_UP,
    getFileNodeUseURL: 'http://' + ip + ':8080/hdatamonitor/api/getFileNodeUse/',//FileNodeUse
    getMultipathStatusURL: 'http://' + ip + ':8080/hdatamonitor/api/getMultipathStatus/',//getMultipathStatus

    //calc history
    getHistoryDayURL: 'http://' + ip + ':8080/hdatamonitor/api/getHistoryDay/',//history day
    getHistoryDayURL_TIME: 1000 * 60 * 30,
    // getHistoryDayURL_TIME:10000,
    getLoadAverageHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getLoadAverageHistoryHour',//load hour
    getCpuHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getCpuHistoryHour/',//cpu hour
    getMemoryHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getMemoryHistoryHour/',//Memory hour
    getIOHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getIOHistoryHour/',//io hour
    getNicHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getNicHistoryHour/',//nic hour
    getHcaHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getHcaHistoryHour/',//hca hour
    //history log
    getLoadAverageHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getLoadAverageHistoryHourGridData',//load hour
    getCpuHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getCpuHistoryHourGridData',//cpu hour
    getMemoryHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getMemoryHistoryHourGridData',//Memory hour
    getIoHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getIoHistoryHourGridData',//io hour
    getNicHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getNicHistoryHourGridData',//nic hour
    getHcaHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getHcaHistoryHourGridData',//hca hour


    // storage
    getPiceHostInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getPiceHostInfo/',//getPiceHostInfo
    getPiceHostInfoURL_TIME: TIME_UP,
    getPiceDataInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getPiceDataInfo/',//getPiceDataInfo
    getPiceControllerInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getPiceControllerInfo/',//getPiceControllerInfo
    getPcieInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieInfo/',//getPcieInfo

    //storage history
    getDayHistoryStorageNodeURL: 'http://' + ip + ':8080/hdatamonitor/api/getDayHistoryStorageNode/',//history day
    getPcieHostHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieHostHistoryHour/',//pcie host hour
    getPcieDataHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieDataHistoryHour/',//pcie data hour
    getPcieControllerHistoryHourURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieControllerHistoryHour/',//pcie Controller hour
    //storage history log
    getPcieHostHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieHostHistoryHourGridData/',//getPcieHostHistoryHourGridData
    getPcieDataHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieDataHistoryHourGridData/',//getPcieDataHistoryHourGridData
    getPcieControllerHistoryHourGridDataURL: 'http://' + ip + ':8080/hdatamonitor/api/getPcieControllerHistoryHourGridData/',//getPcieControllerHistoryHourGridData

    //login
    updatePasswordURL: 'http://' + ip + ':8080/hdatamonitor/api/updatePassword',
    checkLoginUserURL: 'http://' + ip + ':8080/hdatamonitor/api/checkLoginUser',

    //setting
    getConfigureHoldTimeURL: 'http://' + ip + ':8080/hdatamonitor/api/getConfigureHoldTime',
    saveConfigureHoldTimeURL: 'http://' + ip + ':8080/hdatamonitor/api/saveConfigureHoldTime/',
    getThresholdURL: 'http://' + ip + ':8080/hdatamonitor/api/getThreshold',
    registerHdataURL: 'http://' + ip + ':8080/hdatamonitor/api/registerHdata',//register

    //db
    getVSessionSnapURL: 'http://' + ip + ':8080/hdatamonitor/api/getVSessionSnap/',
    getLockTreeURL: 'http://' + ip + ':8080/hdatamonitor/api/getLockTree',
    getAsmDiskgroupInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getAsmDiskgroupInfo',
    getAsmDiskStatusURL: 'http://' + ip + ':8080/hdatamonitor/api/getAsmDiskStatus',
    getRACMonitorInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getRACMonitorInfo',
    getTableSpaceRateURL: 'http://' + ip + ':8080/hdatamonitor/api/getTableSpaceRate',
    getRMANBackupInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getRMANBackupInfo',
    getInstanceNamesURL: 'http://' + ip + ':8080/hdatamonitor/api/getInstanceNames',
    getSnapIntervalTimeURL: 'http://' + ip + ':8080/hdatamonitor/api/getSnapIntervalTime/',
    getHistoricalFailureAnalysisURL: 'http://' + ip + ':8080/hdatamonitor/api/getHistoricalFailureAnalysis',
    getDBSQLInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getDBSQLInfo',
    getDBSQLPlanURL: 'http://' + ip + ':8080/hdatamonitor/api/getDBSQLPlan',
    getSQLPLanExcelURL: 'http://' + ip + ':8080/hdatamonitor/api/getSQLPLanExcel',
    getDBMemoryInfoURL: 'http://' + ip + ':8080/hdatamonitor/api/getDBMemoryInfo',



};

module.exports = components;