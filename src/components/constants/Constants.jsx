
export const LINK_INDEX = "/main";
export const CALC_INDEX = LINK_INDEX + "/calcIndex/";
export const STORAGE_INDEX = LINK_INDEX + "/storageIndex/";
export const LINK_CALC_INDEX = CALC_INDEX + ":id";
export const LINK_STORAGE_INDEX = STORAGE_INDEX + ":id"
export const DB_INDEX = LINK_INDEX + "/db";
export const SETTING_INDEX = LINK_INDEX + "/setting";
export const DB_INDEX_LOCK = LINK_INDEX + "/dbLock";
export const DB_INDEX_MEMORY = LINK_INDEX + "/dbMemory";
export const DB_INDEX_HISTORY = LINK_INDEX + "/dbHistory";
export const DB_INDEX_SQL = LINK_INDEX + "/dbSql";
export const DB_INDEX_ASMSIZE = LINK_INDEX + "/dbAsmSize";
export const DB_INDEX_ASMSTATUS = LINK_INDEX + "/dbAsmStatus";
export const DB_INDEX_CLUSTERQUERY = LINK_INDEX + "/dbClusterQuery";
export const DB_INDEX_TABLESPACE = LINK_INDEX + "/dbTableSpace";
export const DB_INDEX_RMAN = LINK_INDEX + "/dbRMAN";

export const DB_INDEX_LOCK_TEXT = '锁关系图';
export const DB_INDEX_MEMORY_TEXT = '内存使用分布';
export const DB_INDEX_HISTORY_TEXT = '历史故障分析';
export const DB_INDEX_SQL_TEXT = 'SQL语句性能分析';
export const DB_INDEX_ASMSIZE_TEXT = 'ASM磁盘组容量监控';
export const DB_INDEX_ASMSTATUS_TEXT = 'ASM磁盘状态监控';
export const DB_INDEX_CLUSTERQUERY_TEXT = '集群资源查询';
export const DB_INDEX_TABLESPACE_TEXT = '表空间使用率';
export const DB_INDEX_RMAN_TEXT = '数据库RMAN备份监控';

export const dbId_1 = 'db1';
export const dbId_2 = 'db2';
export const dbId_3 = 'db3';
export const dbId_4 = 'db4';
export const dbId_5 = 'db5';
export const dbId_6 = 'db6';
export const dbId_7 = 'db7';
export const dbId_8 = 'db8';
export const dbId_9 = 'db9';

export const WARN = "warn";
export const ERROR = "error";
export const WARN_MSG = "警告";
export const ERROR_MSG = "错误";

export const COMPUTENODE = "computenode";
export const STORAGENODE = "storagenode";

export const DELETE_MONITORLOG_TITLE = "确定要删除这些日志吗？";


export const WATCH_LEFT_TITLE = "CPU负载";
export const WATCH_MAIN_TOP_TITLE = "HDATA 高性能ORACLE RAC集群";
export const WATCH_MAIN_LOADAVERAGE_TITLE = "Load Average";
export const WATCH_MAIN_SYSTEM_TITLE = "系统可用性";
export const WATCH_MAIN_TOP5EVENT_TITLE = "TOP等待事件";
export const WATCH_MAIN_DIALOG_TITLE = "会话数";
export const WATCH_RIGHT_IO = "I/O负载";
export const WATCH_RIGHT_NET = "网络负载";


export const CALC_LOAD_AVERAGE = "Load Average";
export const CPU_STATUS = "CPU状态";
export const MEMORY_STATUS = "内存状态";
export const IO_STATUS = "I/O状态";

export const PID_CPU = "PID:CPU Top 5";
export const PID_MEMORY = "PID:Memory Top 5";
export const PID_IO = "PID:I/O Top 5";
export const PUBLIC = "PUBLIC网卡";
export const PRIVATE = "Private网卡";
export const HCA = "HCA卡";

export const RCP1 = "RCP1";
export const RCP1_IP = "192.168.100.132";
export const LAST_RUN_TIME = "2017/06/01 09:25:00";
export const PRIMART_NAME = "ORACLE Linux 7.2.1";
export const PRIMART_NET = "Intel(R) Core(TM)i7-6500U 2.50GHz4*2";
export const PRIMARY_SIZE = "128G";

export const PCIE_HOST = "PCI-E Host R/W Commands";
export const PCIE_DATA = "PCI-E Data Read/Write";
export const PCIE_CONTROLLER = "PCI-E Controller Busy Time";

export const MountPointUse = "挂载点使用率";
export const FileNodeUse = "File Nodes使用率";
export const StorageNode = "存储节点映射磁盘状态";

export const PCE_E_MSG = "PCI-E信息";

export const THRESHOLD_CPU = "cup-status";
export const THRESHOLD_CPU_TEXT = "CPU状态";
export const THRESHOLD_SESSION = "session-amount";
export const THRESHOLD_SESSION_TEXT = "会话数";
export const THRESHOLD_LOAD = "load-average";
export const THRESHOLD_LOAD_TEXT = "平均负载";
export const THRESHOLD_MEMORY = "memory-use";
export const THRESHOLD_MEMORY_TEXT = "内存使用情况";
export const THRESHOLD_MOUNT = "mount-point";
export const THRESHOLD_MOUNT_TEXT = "挂载点使用率";
export const THRESHOLD_INODES = "inodes-use";
export const THRESHOLD_INODES_TEXT = "inodes使用率";

export const DBINDEX_SESSION_TEXT = "活动会话";

export const DBINDEX_MONITOR_OTHER = "Other";
export const DBINDEX_MONITOR_OTHER_TEXT = "其他";
export const DBINDEX_MONITOR_APPLICATION = "Application";
export const DBINDEX_MONITOR_APPLICATION_TEXT = "应用程序";
export const DBINDEX_MONITOR_CONFIGURATION = "Configuration";
export const DBINDEX_MONITOR_CONFIGURATION_TEXT = "配置";
export const DBINDEX_MONITOR_ADMINISTRATIVE = "Administrative";
export const DBINDEX_MONITOR_ADMINISTRATIVE_TEXT = "管理";
export const DBINDEX_MONITOR_CONCURRENCY = "Concurrency";
export const DBINDEX_MONITOR_CONCURRENCY_TEXT = "并发";
export const DBINDEX_MONITOR_COMMIT = "Commit";
export const DBINDEX_MONITOR_COMMIT_TEXT = "提交";
export const DBINDEX_MONITOR_IDLE = "Idle";
export const DBINDEX_MONITOR_IDLE_TEXT = "闲置";
export const DBINDEX_MONITOR_NETWORK = "Network";
export const DBINDEX_MONITOR_NETWORK_TEXT = "网络";
export const DBINDEX_MONITOR_USERIO = "User I/O";
export const DBINDEX_MONITOR_USERIO_TEXT = "用户I/O";
export const DBINDEX_MONITOR_SYSTEMIO = "System I/O";
export const DBINDEX_MONITOR_SYSTEMIO_TEXT = "系统I/O";
export const DBINDEX_MONITOR_SCHEDULER = "Scheduler";
export const DBINDEX_MONITOR_SCHEDULER_TEXT = "调度程序";
export const DBINDEX_MONITOR_CLUSTER = "Cluster";
export const DBINDEX_MONITOR_CLUSTER_TEXT = "集群";
export const DBINDEX_MONITOR_QUEUEING = "Queueing";
export const DBINDEX_MONITOR_QUEUEING_TEXT = "队列";
export const DBINDEX_MONITOR_CPU = "OnCpu";
export const DBINDEX_MONITOR_CPU_TEXT = "CPU";

//db
export const DBINDEX_ASMSIZE_LEGEND = "usable";
export const DBINDEX_ASMSIZE_LEGEND_USABLE_TEXT = "已用空间";
export const DBINDEX_ASMSIZE_LEGEND_NOTUSED_TEXT = "可用空间";


export const DBINDEX_ASMSTATUS_STATUS_TEXT = "normal";
export const DBINDEX_ASMSTATUS_LEGEND_TEXT1 = "正常状态";
export const DBINDEX_ASMSTATUS_LEGEND_TEXT2 = "异常状态";
export const DBINDEX_ASMSTATUS_DATA_VALUE1 = "h1";
export const DBINDEX_ASMSTATUS_DATA_VALUE2 = "h2";
export const DBINDEX_ASMSTATUS_DATA_VALUE3 = "h3";
export const DBINDEX_ASMSTATUS_DATA_VALUE1_TEXT = "存储节点1";
export const DBINDEX_ASMSTATUS_DATA_VALUE2_TEXT = "存储节点2";
export const DBINDEX_ASMSTATUS_DATA_VALUE3_TEXT = "存储节点3";
