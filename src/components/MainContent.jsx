import React from 'react';
import {Layout} from 'antd';

const {Content, Sider} = Layout;
import {HashRouter as Router,Route,Link} from 'react-router-dom';

import * as Constants from './constants/Constants';

import Watch from "./watch/Watch";
import CalcIndex from "./calc/CalcIndex";
import StorageIndex from "./storage/StorageIndex";

import DbIndex from "./db/DbIndex";
import AsmSize from "./db/AsmSize";
import AsmStatus from "./db/AsmStatus";
import ClusterQuery from "./db/ClusterQuery";
import TableSpace from "./db/TableSpace";
import DBRMAN from "./db/DBRMAN";
import MemoryInfo from "./db/MemoryInfo";
import HistoryFault from "./db/HistoryFault";
import SQLAnalyze from "./db/SQLAnalyze";
import Lock from "./db/Lock";

import SettingIndex from "./setting/SettingIndex";

import Icon1_NOSelect from '../img/icon1-status1.png';
import Icon1_Select from '../img/icon1-status2.png';
import Icon2_NOSelect from '../img/icon2-status1.png';
import Icon2_Select from '../img/icon2-status2.png';
import Icon3_NOSelect from '../img/icon3-status1.png';
import Icon3_Select from '../img/icon3-status2.png';

export default class MainContent extends React.Component {
    onChangeStatus1 = (value) => {
        this.props.onChangeStatus(value);
    };

    render() {
        if (this.props.menu.length === 0 && this.props.view === 1) {//no data for deviceInfo
            return null;
        }
        console.log('MainContent......');

        let height = 'calc(100% - 64px)';
        let menuWidth = '186';
        let menu_margin = '0px 0px 0px '+menuWidth+'px';
        if (document.body.clientWidth > 1400) {
            height = '100%';
            menuWidth = 200;
            menu_margin = '0px 0px 0px '+menuWidth+'px';
        };
        const that = this;

        //init menu
        const computenode = [];
        const storagenode = [];

        this.props.menu.map(function (item, index) {
            const id = 'menu' + item.id;
            let link = Constants.CALC_INDEX + item.id;

            if (item.deviceType === Constants.COMPUTENODE) {
                computenode.push(
                    <li id={id} key={id}
                        className={that.props.id === id ? 'selected hdata-menu-item-content' : 'hdata-menu-item-content'}>
                        <Link to={link} onClick={that.onChangeStatus1.bind(that, id)}>
                            <img alt=""
                                 src={that.props.id === id ? Icon1_Select : Icon1_NOSelect}/><span>{item.hostName}</span>
                        </Link>
                    </li>
                )
            } else {
                link = Constants.STORAGE_INDEX + item.id;
                storagenode.push(
                    <li id={id} key={id}
                        className={that.props.id === id ? 'selected hdata-menu-item-content' : 'hdata-menu-item-content'}>
                        <Link to={link} onClick={that.onChangeStatus1.bind(that, id)}>
                            <img alt=""
                                 src={that.props.id === id ? Icon2_Select : Icon2_NOSelect}/><span>{item.hostName}</span>
                        </Link>
                    </li>
                )
            }
        });

        const dbTree = [];

        const dbMenu = [
            {'id': Constants.dbId_1, 'link': Constants.DB_INDEX_ASMSIZE, 'title': Constants.DB_INDEX_ASMSIZE_TEXT},
            {'id': Constants.dbId_2, 'link': Constants.DB_INDEX_ASMSTATUS, 'title': Constants.DB_INDEX_ASMSTATUS_TEXT},
            {'id': Constants.dbId_3, 'link': Constants.DB_INDEX_CLUSTERQUERY, 'title': Constants.DB_INDEX_CLUSTERQUERY_TEXT},
            {'id': Constants.dbId_4, 'link': Constants.DB_INDEX_TABLESPACE, 'title': Constants.DB_INDEX_TABLESPACE_TEXT},
            {'id': Constants.dbId_5, 'link': Constants.DB_INDEX_RMAN, 'title': Constants.DB_INDEX_RMAN_TEXT},
            {'id': Constants.dbId_6, 'link': Constants.DB_INDEX_MEMORY, 'title': Constants.DB_INDEX_MEMORY_TEXT},
            {'id': Constants.dbId_7, 'link': Constants.DB_INDEX_HISTORY, 'title': Constants.DB_INDEX_HISTORY_TEXT},
            {'id': Constants.dbId_8, 'link': Constants.DB_INDEX_SQL, 'title': Constants.DB_INDEX_SQL_TEXT},
            {'id': Constants.dbId_9, 'link': Constants.DB_INDEX_LOCK, 'title': Constants.DB_INDEX_LOCK_TEXT}
        ];

        dbMenu.forEach(function (item, index) {
            dbTree.push(
                <li id={item.id} key={item.id}
                    className={that.props.id === item.id ? 'selected hdata-menu-item-content menu-db-content-font-size' : 'hdata-menu-item-content menu-db-content-font-size'}>
                    <Link to={item.link} onClick={that.onChangeStatus1.bind(that, item.id)}>
                        <img alt=""
                             src={that.props.id === item.id ? Icon3_Select : Icon3_NOSelect}/><span>{item.title}</span>
                    </Link>
                </li>
            );
        });
        //init menu end....

        return (
            <Layout>
                <Sider width={menuWidth} style={{backgroundColor: '#444b5b', height: height, position: 'fixed', overflow: 'auto'}}>
                    <ul className="hdata-menu">
                        <li id="sub1" key="sub1" className={this.props.id === 'sub1' ? 'selected-title blue' : 'blue'} >
                            <Link to={Constants.LINK_INDEX}
                                  onClick={this.onChangeStatus1.bind(this, 'sub1')}><span>仪表盘</span></Link>
                        </li>

                        <li id="sub2" key="sub2" className="green">
                            <span>计算节点</span>
                        </li>

                        {computenode}

                        <li id="sub3" key="sub3" className="orange"><span>存储节点</span></li>

                        {storagenode}

                        <li id="sub4" key="sub4"
                            className={this.props.id === 'sub4' ? 'selected-title orange-yellow' : 'orange-yellow'}>
                            <Link to={Constants.DB_INDEX} onClick={this.props.onChangeStatus.bind(this, 'sub4')}>
                                <span>数据库</span>
                            </Link>
                        </li>

                        {dbTree}

                        <li id="sub5" key="sub5"
                            className={this.props.id === 'sub5' ? 'selected-title setting-color' : 'setting-color'}>
                            <Link to={Constants.SETTING_INDEX} onClick={this.props.onChangeStatus.bind(this, 'sub5')}>
                                <span>设置</span>
                            </Link>
                        </li>
                    </ul>
                </Sider>
                <Layout>
                    <Content style={{background: '#F7F7F7', padding: 0, margin: menu_margin}}>
                        <Route exact path={Constants.LINK_INDEX} component={Watch}/>
                        <Route path={Constants.LINK_CALC_INDEX} component={CalcIndex}/>
                        <Route path={Constants.LINK_STORAGE_INDEX} component={StorageIndex}/>
                        <Route path={Constants.DB_INDEX} component={DbIndex}/>
                        <Route path={Constants.SETTING_INDEX} component={SettingIndex}/>
                        <Route path={Constants.DB_INDEX_ASMSIZE} component={AsmSize}/>
                        <Route path={Constants.DB_INDEX_ASMSTATUS} component={AsmStatus}/>
                        <Route path={Constants.DB_INDEX_CLUSTERQUERY} component={ClusterQuery}/>
                        <Route path={Constants.DB_INDEX_TABLESPACE} component={TableSpace}/>
                        <Route path={Constants.DB_INDEX_RMAN} component={DBRMAN}/>
                        <Route path={Constants.DB_INDEX_MEMORY} component={MemoryInfo}/>
                        <Route path={Constants.DB_INDEX_HISTORY} component={HistoryFault}/>
                        <Route path={Constants.DB_INDEX_SQL} component={SQLAnalyze}/>
                        <Route path={Constants.DB_INDEX_LOCK} component={Lock}/>
                    </Content>
                </Layout>
            </Layout>
        )
    }
}