import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import EchartsUtils from '../common/EchartsUtils';
import echarts from 'echarts/lib/echarts';
import 'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';


// pid pile graph
export default class PidPileBar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pidPileBar: []
        }
    };

    componentDidMount = () => {
        console.log('-----------------'+ this.props.id)
        this.init(this.props.url);
    };
    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID, echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        EchartsUtils.handler(echarts, this.props.id);
    };

    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: 'get'
            ,type: 'json'
            , contentType: 'application/json'
            , crossOrigin: true
            , withCredentials: true
        }).then((resp) => {
            if (resp.success) {
                console.log(resp);
                this.setState({pidPileBar: this.format(resp.data)});
                echarts.getInstanceByDom(document.getElementById(this.props.id)) == undefined ? this.initPie() : this.dynamic();
            } else {
                console.log(params.url);
                console.log(resp);
            }
        });


    };
    init = (url) => {
        this.fetch({
            url: url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getMonitorLoginfoURL_TIME
        );
    };
    initPie = () => {
        if (this.state.pidPileBar === undefined || this.state.pidPileBar.length == 0) {
            return null;
        }
        let myChart = echarts.init(document.getElementById(this.props.id));
        let options = this.setPieOption(this.state.pidPileBar);
        //设置options
        myChart.setOption(options);

        window.addEventListener("resize", this.handler, false);
    };
    dynamic = () => {
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        let option = myChart.getOption();
        option.series[0].data = this.state.pidPileBar.sysEventList;

        myChart.setOption(option);
    };
    format = (str) =>{
        for(var i in str.sysEventList){
            str.sysEventList[i].value = str.sysEventList[i].timeWaitedMicro;
        }
        return str;
    };
    setPieOption = (data) => {
        let xdata = [];
        for(var i in data.sysEventList){
            xdata.push(data.sysEventList[i].timeWaitedMicro)
        }

        return {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                },
                textStyle: {
                    align: 'left'
                },
                position: ['50%', -50],
                formatter: function (param) {
                    param = param[0];

                    return [
                        param.seriesName + '<hr size=1 style="margin: 3px 0">',
                        '-EventName :&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.eventName + '<br/>',
                        '-WaitedTime:&nbsp;&nbsp;&nbsp;&nbsp;' + param.data.timeWaitedMicro + 'ms<br/>'
                    ].join('');
                }
            },
            legend: {
                show: false,
            },
            grid: {
                show: true,
                top: '10',
                left: '20',
                width: '75%',
                height: '80%',
                borderColor: '#ccc',
                borderWidth: 0,
                containLabel: false
            },
            xAxis: {
                show: false,
                type: 'category',
                // data: ['sshd', 'fxp', 'agent', '', ''],
                data:xdata,
                axisLabel: {
                    textStyle: {
                        color: '#666',
                        fontSize: 14
                    }
                }
            },
            yAxis: {
                type: 'value',
                show: false,
                // max: 100
            },
            series: [{
                name: data.sampleTime + '   ' + data.hostAddress,
                type: 'bar',
                stack: 'value',
                label: {
                    normal: {
                        position: 'insideRight'
                    }
                },
                // barWidth:28,
                // barMaxWidth:40,
                // barGap:'40%',
                // data: [{value: 80, name: '%CPU', pid: '8833', time: '2016-01-01', command: 'sshd1'}
                //     , {value: 76, name: '%CPU', pid: '8833', time: '2016-01-01', command: 'sshd2'}
                //     , {value: 45, name: '%CPU', pid: '8833', time: '2016-01-01', command: 'sshd3'}
                //     , {value: 22, name: '%CPU', pid: '8833', time: '2016-01-01', command: 'sshd4'}
                //     , {value: 10, name: '%CPU', pid: '8833', time: '2016-01-01', command: 'sshd5'}],
                data:data.sysEventList

            }],
            color: ['#ED5555']
        }
    };

    render() {
        // console.log('render')
        return (
            <div id={this.props.id} style={{width: "99%", minHeight: this.props.minHeight}}></div>
        );
    }
}



