import React from 'react';

import WatchLineIo from '../container/WatchLineIo';
import Properties from '../constants/Properties';
import reqwest from 'reqwest';

var url = Properties.getAllHCAURL;
var point = Properties.cpuInterval / Properties.getAllCpuStatusURL_TIME;

export default class WatchCpuService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            watchLineData: []
        }
    };

    componentDidMount = () => {
        this.fetch({
            url: url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getAllCpuStatusURL_TIME
        );
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    };

    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log('watchCpuService...fetch....')
            console.log(resp)
            if (resp.success) {
                let result = [];
                for(let i in resp.dataList){
                    const data = resp.dataList[i];
                    // console.log(data)
                    // data.xdata = [];
                    // data.loadLineList.text = [];
                    if (data.xdata.length < point) {
                        const c = getNowFormatDate();
                        const diff = point - data.xdata.length;

                        for (let i = 0; i < diff; i++) {//minute point
                            // data.xdata.unshift('-');
                            data.xdata.unshift(c);
                        }

                        data.loadLineList.forEach(function (value, index, array) {
                            for (let i = 0; i < diff; i++) {
                                // value.text.unshift({value:'0', time: '-'})
                                value.text.unshift({value: '0', time: c})
                            }
                        });
                    }
                    result.push(data);
                }

                this.setState({watchLineData: result});

                this.setState({watchLineData: resp.dataList});
            } else {
                console.log(params.url)
                console.log(resp);
            }
        });
    };

    render() {
        if (this.state.watchLineData.length === 0) {
            return null;
        }

        console.log('watchIoService....');

        let io_line_color = ['#FFCE57', '#71D9C0', '#F38D3A'];
        let io_areaStyle = ['#FFF5DF', '#F0FBF9', '#FEF7F1'];

        const netList = [
            {"id": "net1", "minHeight": "90px"},
            {"id": "net2", "minHeight": "90px"}
        ];

        let data = this.state.watchLineData;

        return (
            <div>
                {/*<div className="flex-item1">*/}
                    {/*/!*<WatchLineTps url={Properties.getAllHCAURL} id="net1" listIndex="0" minHeight="90px" lineColor={io_line_color} areaStyle={io_areaStyle}/>*!/*/}
                {/*</div>*/}
                {/*<div className="flex-item2">*/}
                    {/*/!*<WatchLineTps url={Properties.getAllHCAURL} id="net2" listIndex="1" minHeight="90px" lineColor={io_line_color} areaStyle={io_areaStyle}/>*!/*/}
                {/*</div>*/}
                {
                    netList.map(function (item, index) {
                        let classTemp = '';
                        if (index === 0) {
                            classTemp = 'flex-item1';
                        } else if (index === 1) {
                            classTemp = 'flex-item2';
                        }
                        return (
                            <div className={classTemp} key={index}>
                                <WatchLineIo id={item.id}
                                              minHeight={item.minHeight}
                                              // listIndex={index}
                                              // url={Properties.getALlIOLoadURL}
                                              watchLineData={data[index]}
                                              lineColor={io_line_color}
                                              areaStyle={io_areaStyle}/>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}
function getNowFormatDate() {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    let getHours = date.getHours()
    let getMinutes = date.getMinutes()
    let getSeconds = date.getSeconds()
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    if (getHours >= 1 && getHours <= 9) {
        getHours = "0" + getHours;
    }
    if (getMinutes >= 0 && getMinutes <= 9) {
        getMinutes = "0" + getMinutes;
    }
    if (getSeconds >= 0 && getSeconds <= 9) {
        getSeconds = "0" + getSeconds;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + getHours + seperator2 + getMinutes
        + seperator2 + getSeconds;
    return currentdate;
}