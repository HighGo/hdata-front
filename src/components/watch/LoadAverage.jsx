import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';
import EchartsUtils from '../common/EchartsUtils';

import echarts from 'echarts/lib/echarts';
import  'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';
import 'echarts/lib/component/legend';


var colorList = [
    '#c1913e', '#3e88d2', '#3aaac7', '#86c433', '#f38d3a'
];

export default class LoadAverage extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loadAverage: [],
        }
    };
    componentDidMount = () => {
        this.fetch({
            url: this.props.url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: this.props.url,
                method: 'get'
            }),
            Properties.getLoadAverageURL_TIME
        );
    };
    componentWillUnmount() {
        EchartsUtils.Unmount(this.timerID, echarts, this.props.id);
        window.removeEventListener("resize", this.handler, false);
    };

    handler = () => {
        EchartsUtils.handler(echarts, this.props.id);
    };

    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp)
            if (resp.success) {
                this.setState({
                    loadAverage: resp.data
                });
                this.initPie();
            }else{
                console.log(params.url)
                console.log(resp)
            }
        });
    };
    initPie = () => {//init
        let myChart = echarts.getInstanceByDom(document.getElementById(this.props.id));
        if (myChart === undefined) {
            myChart = echarts.init(document.getElementById(this.props.id));
        }

        let options = this.setPieOption(this.state.loadAverage);
        //legend select changed
        myChart.on('legendselectchanged', function (params) {
            options.series.forEach(function (e) {
                if (e.name === params.name) {
                    options.legend.selected[e.name] = true;
                } else {
                    options.legend.selected[e.name] = false;
                }
            });
            myChart.setOption(options);
        });
        //reset option
        myChart.setOption(options)

        window.addEventListener("resize", this.handler, false);
    };
    setPieOption = (data) => {

        let series = [];
        let xdata = data.xdata;
        data.loadLineList.forEach(function (value, index, array) {
            series.push({
                type: 'bar',
                name: value.name,
                data: value.text,
                barWidth: '60%',
                itemStyle: {
                    normal: {
                        color: function (params) {
                            return colorList[params.dataIndex];
                        }
                    },
                    //The mouse is suspended
                    emphasis: {
                        shadowBlur: 10,
                        shadowOffsetX: 0,
                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                    }
                },
                label: {
                    normal: {
                        show: true,
                        formatter: '{c}',
                        position: 'top'
                    }
                },
                markPoint: {
                    label: {
                        normal: {
                            show: true
                        }
                    },
                    data: [
                        {
                            name: '',
                            x: 100,
                            y: 100
                        }
                    ]
                }
            });
        });
        return {
            grid: {
                show: true,
                top: 14,
                left: 'left',
                // bottom:'100',
                width: '88%',
                height: '80%',
                borderColor: '#ccc',
                borderWidth: 0,
                containLabel: true
            },
            tooltip: {
                formatter: '{a}: {c}'
            },
            legend: {
                show: true,
                left: 'right', // 'center' | 'left' | {number},
                top: 'middle', // 'center' | 'bottom' | {number}
                // right:'20px',
                // width:'10px',
                orient: 'vertical',//，horizontal|vertical
                backgroundColor: 'white',
                borderColor: '#eee',
                borderWidth: 2,
                selected: {
                    '1min': false,
                    '5min': true,
                    '15min': false
                },
                data: [
                    {
                        icon: 'none',
                        name: '1min',
                        textStyle: {color: '#fba71a'},
                    },
                    {
                        name: '5min',
                        icon: 'none',
                        // icon: 'circle',
                        textStyle: {color: '#fba71a'}
                    },
                    {
                        // icon: 'image://../images/icon-time1.png',
                        name: '15min',
                        icon: 'none',
                        textStyle: {color: '#fba71a'},
                    }
                ]
            },
            xAxis: {
                axisLabel: {
                    show: true,
                    interval: 0,    // {number}
                    // rotate: 20,
                    textStyle: {
                        // color: 'blue',
                        // fontFamily: 'sans-serif',
                        fontSize: 12,
                        fontStyle: 'italic',
                        // fontWeight: 'bold'
                    },
                    margin: 10
                },
                show: true,
                type: 'category',
                data: xdata,
                // boundaryGap: true
            },
            yAxis: {
                show: false,
                // max: 130
            },
            series: series,
            color: ['#ffbf00', '#7265e6']
        }
    };

    render() {
        return (
            <div style={{margin: '8px 10px'}}>
                <div id={this.props.id} style={{width: "99%", minHeight: '125px'}}></div>
            </div>
        );
    }
}



