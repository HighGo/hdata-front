import React from 'react';

import WatchLine from '../container/WatchLine';
import Properties from '../constants/Properties';
import reqwest from 'reqwest';

var url = Properties.getAllCpuStatusURL;
var point = Properties.cpuInterval / Properties.getAllCpuStatusURL_TIME;

export default class WatchCpuService extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            watchLineData: []
        }
    };

    componentDidMount = () => {
        this.fetch({
            url: url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getAllCpuStatusURL_TIME
        );
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    };

    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log('watchCpuService...fetch....');
            console.log(resp);
            if (resp.success) {
                let result = [];
                for (let i in resp.dataList) {
                    const data = resp.dataList[i];
                    // console.log(data)
                    // data.xdata = [];
                    // data.loadLineList.text = [];
                    if (data.xdata.length < point) {
                        const c = getNowFormatDate();
                        const diff = point - data.xdata.length;

                        for (let i = 0; i < diff; i++) {//minute point
                            // data.xdata.unshift('-');
                            data.xdata.unshift(c);
                        }

                        data.loadLineList.forEach(function (value, index, array) {
                            for (let i = 0; i < diff; i++) {
                                // value.text.unshift({value:'0', time: '-'})
                                value.text.unshift({value: '0', time: c})
                            }
                        });
                    }
                    result.push(data);
                }

                this.setState({watchLineData: result});
            } else {
                console.log(params.url)
                console.log(resp);
            }
        });
    };

    render() {
        if (this.state.watchLineData.length === 0) {
            return null;
        }

        console.log('watchCpuService....');

        let cpu_line_color = ['#70D9C0', '#FFCE57', '#EC8CC2'];
        let cpu_areaStyle = ['#EFF4FC', '#F2FAFB', '#F2FBF8'];
        const cpuList = [
            {"id": "watchLine1", "minHeight": "100px"}
            , {"id": "watchLine2", "minHeight": "100px"},
            {"id": "watchLine3", "minHeight": "100px"},
            {"id": "watchLine4", "minHeight": "100px"},
            {"id": "watchLine5", "minHeight": "100px"},
        ];

        let data = this.state.watchLineData;

        return (
            <div>
                {
                    cpuList.map(function (item, index) {
                        let styleType = {};
                        if (index === cpuList.length - 1) {
                            styleType = {marginBottom: '0px'};
                        }
                        return (
                            <div className="flex-item" style={styleType} key={index}>
                                <WatchLine id={item.id}
                                           minHeight={item.minHeight}
                                           lineColor={cpu_line_color}
                                           areaStyle={cpu_areaStyle}
                                    // listIndex={index}
                                           watchLineData={data[index]}
                                />
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}

function getNowFormatDate() {
    let date = new Date();
    let seperator1 = "-";
    let seperator2 = ":";
    let month = date.getMonth() + 1;
    let strDate = date.getDate();
    let getHours = date.getHours()
    let getMinutes = date.getMinutes()
    let getSeconds = date.getSeconds()
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }

    if (getHours >= 1 && getHours <= 9) {
        getHours = "0" + getHours;
    }
    if (getMinutes >= 0 && getMinutes <= 9) {
        getMinutes = "0" + getMinutes;
    }
    if (getSeconds >= 0 && getSeconds <= 9) {
        getSeconds = "0" + getSeconds;
    }
    let currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
        + " " + getHours + seperator2 + getMinutes
        + seperator2 + getSeconds;
    return currentdate;
}