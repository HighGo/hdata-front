import React from 'react';
import Title from '../common/Title';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';

var colorList = [
    '#919191', '#3aaac7', '#6fb955', '#f38d3a',
];

var colorList2 = {
    error:'#919191',
    computenode:'#3aaac7',
    storagenode:'#6fb955',
    switchboard:'#f38d3a'}

export default class SystemUse extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            systemUse: [],
        }
    }
    componentDidMount = () => {
        this.fetch({
            url: this.props.url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: this.props.url,
                method: 'get'
            }),
            Properties.getSystemAvailabilityURL_TIME
        );

    };
    componentWillUnmount() {
        clearInterval(this.timerID);
    };
    fetch = (params = {}) => {
        reqwest({
            url: params.url,
            method: params.method,
            data: {
                ...params,
            }
            , type: 'json'
            , contentType: 'application/json'
        }).then((resp) => {
            console.log(resp)
            if (resp.success) {
                this.setState({
                    systemUse: resp.data
                });
            }else {
                console.log(params.url);
                console.log(resp);
            }
        });
    };

    render() {
        if(this.state.systemUse.length === 0){
            return null;
        }
        return (
            <div className="SystemUse">
                <Title title={this.props.title}/>
                <ItemList data={this.state.systemUse.data}/>
                <LegendList titles={this.state.systemUse.title}/>
            </div>
        )
    }
}

class ItemList extends React.Component {
    render() {
        return (
            <div className="item-list">
                {
                    this.props.data.map(function (item, index) {
                        if (item.status !== Properties.getSystemAvailabilityURL_RUNNING) {
                            return <Item key={item.id} item={item} color={colorList2['error']}/>//status is error
                        } else {
                            return <Item key={item.id} item={item} color={colorList2[item.type]}/>
                        }
                    })
                }
            </div>
        )
    }
}


class Item extends React.Component {
    render() {
        return (
            <div className="item" style={{backgroundColor: this.props.color}}>
                {this.props.item.name}
            </div>
        )
    }
}


class LegendList extends React.Component {
    render() {
        return (
            <div className="legend-list">
                {
                    this.props.titles.map(function (item, index) {
                        return <Legend key={index} item={item} color={colorList[index + 1]}/>
                    })
                }
            </div>
        )
    }
}

class Legend extends React.Component {
    render() {
        return (
            <div className="item">
                <div className="line" style={{backgroundColor: this.props.color}}>&nbsp;</div>
                {this.props.item}
            </div>
        )
    }
}