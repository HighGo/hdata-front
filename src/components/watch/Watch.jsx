import React from 'react';

import '../../css/watch.css';

import WatchCpuService from './WatchCpuService';
import WatchIoService from './WatchIoService';
import WatchNetService from './WatchNetService';
import LoadAverage from './LoadAverage'
import SystemUse from './SystemUse'
// import * as config from '../../data/echarts.json';
import WatchTbale from '../common/WatchTable';

import Title from '../common/Title';
import * as constants from '../constants/Constants';

import GroupTop from './GroupTop';
import GroupDialog from './GroupDialog';
import Properties from '../constants/Properties';

export default class Watch extends React.Component {
    render() {
        console.log('watch....');

        return (
            <div className="container">
                <div className="header">
                    <div className="left">
                        <Title title={constants.WATCH_LEFT_TITLE}/>
                        <div className="panel">
                            <WatchCpuService/>
                        </div>
                    </div>
                    <div className="main">
                        <div className="title">
                            {constants.WATCH_MAIN_TOP_TITLE}
                        </div>
                        <div className="flex-item">
                            <Title title={constants.WATCH_MAIN_LOADAVERAGE_TITLE}/>
                            <LoadAverage title={constants.WATCH_MAIN_LOADAVERAGE_TITLE}
                                         url={Properties.getLoadAverageURL}
                                         id="LoadAverage"
                            />
                        </div>
                        <div className="flex-item">
                            <SystemUse title={constants.WATCH_MAIN_SYSTEM_TITLE}
                                       url={Properties.getSystemAvailabilityURL}/>
                        </div>
                        <div className="flex-item-group">
                            <GroupTop title={constants.WATCH_MAIN_TOP5EVENT_TITLE} id="event"/>
                            <GroupDialog title={constants.WATCH_MAIN_DIALOG_TITLE} id="dialogue"/>
                        </div>
                    </div>
                    <div className="right">
                        <div className="io">
                            <Title title={constants.WATCH_RIGHT_IO}/>
                            <WatchIoService/>
                        </div>
                        <div className="net">
                            <Title title={constants.WATCH_RIGHT_NET}/>
                            <WatchNetService/>
                        </div>
                    </div>
                </div>
                <div className="footer">
                    <WatchTbale url={Properties.getMonitorLoginfoURL}/>
                </div>
            </div>
        )
    }
}