import React from 'react';
import Title from '../common/Title';
import PidPileBar from './PidPileBar';
import Properties from '../constants/Properties';
import reqwest from 'reqwest';

import imgDemo from '../../img/img1.png'

export default class GroupTop extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            nodeList: []
        }
    }

    componentDidMount = () => {
        let url = Properties.getSystemAvailabilityURL;
        this.fetch({
            url: url,
            method: 'get'
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url,
                method: 'get'
            }),
            Properties.getAllCpuStatusURL_TIME
        );
    };
    componentWillUnmount() {
        clearInterval(this.timerID);
    };
    fetch = (params = {}) => {
        reqwest({
            url: params.url
            , method: params.method
            , type: 'json'
            , contentType: 'application/json'

        }).then((resp) => {
            console.log(resp)
            if (resp.success) {
                if (this.state.nodeList.length === 0) {
                    const node = [];
                    for (var i in resp.data.data) {
                        if (resp.data.data[i].type === 'computenode') {
                            node.push(resp.data.data[i])
                        }
                    }
                    this.setState({nodeList: node});
                }
            } else {
                console.log(params.url);
                console.log(resp);
            }
        });
    };

    render() {
        if(this.state.nodeList.length === 0){
            return null;
        }
        console.log('groupTop....');
        return (
            <div className="group">
                <Title title={this.props.title}/>
                {/*<img alt="" src={imgDemo} width="100%"/>*/}
                {
                    this.state.nodeList.map(function (item) {
                        return (
                            <div style={{float: 'left', width: '50%'}} key={item.id}>
                                <PidPileBar id={item.type + item.id}
                                            url={Properties.getDatabaseSysEventURL + item.address}
                                            // url={Properties.getDatabaseSysEventURL + '192.168.100.101'}
                                            minHeight="130px"/>
                            </div>
                        )
                    })
                }

                {/*<div style={{float:'left',width:'50%'}}>*/}
                {/*<PidPileBar id={this.props.id + "top5_2"} url={Properties.getCpuTopPidURL +'1'} minHeight="130px"/>*/}
                {/*</div>*/}
            </div>
        )
    }
}