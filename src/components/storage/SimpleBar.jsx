import React from 'react';



import echarts from 'echarts/lib/echarts';
import  'echarts/lib/chart/bar';
import 'echarts/lib/component/tooltip';

var myChart;


export default class SimpleBar extends React.Component {
    constructor(props) {
        super(props)
    }
    componentWillUnmount() {//销毁执行
        window.removeEventListener('resize', this.onWindowResize);
    };
    componentDidMount = () => {
        this.initPie();
        window.addEventListener('resize', this.onWindowResize);
    };
    componentDidUpdate = () => {//数据改变后
        this.initPie();
        window.addEventListener('resize', this.onWindowResize);
    };
    onWindowResize = () => {
        myChart.resize();
    };
    initPie = () => {
        const data = this.props.data;
        myChart = echarts.init(document.getElementById(this.props.id));
        let options = this.setPieOption(data);
        //设置options
        myChart.setOption(options)
    };
    setPieOption = (data) => {
        // let series = data.series.map(function (item) {
        //     return({
        //         name: item.name,
        //         type: 'bar',
        //         stack: 'value',
        //         label: {
        //             normal: {
        //                 // show: true,
        //                 position: 'insideRight'
        //             }
        //         },
        //         data: item.text
        //     })
        // })

        return {
            tooltip : {
                trigger: 'axis',
                axisPointer : {            // 坐标轴指示器，坐标轴触发有效
                    type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                }
            },
            grid: {
                show: false,
                top: '0',
                left: '0',
                width: '55%',
                height: '90%',
                containLabel: true
            },
            xAxis:  {
                show:true,
                type: 'category',
                data: ['Device1','Device2','Device3'],
                axisLine:{show:false},
                axisTick:{show:false}
            },
            yAxis: {
                show:false,
                type: 'value',
                max:130
            },
            series:
                {
                    name: '直接访问',
                    type: 'bar',
                    stack: '总量',
                    label: {
                        normal: {
                            show: false,
                            position: 'insideRight'
                        }
                    },
                    // barWidth:50,
                    data: [90, 60, 50],
                    color:['#6fb955']
                }
            ,

        }
    };

    render() {
        return (
            <div id={this.props.id}
                 style={{width: "calc(100% - 17px)", minHeight: this.props.minHeight, margin: '0px auto'}}></div>

        );
    }
}



