import React from 'react';
import reqwest from 'reqwest';
import Properties from '../constants/Properties';

export default class PCIETable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: []
        }
    };

    componentDidMount() {
        this.init(this.props.url);
    };

    componentWillReceiveProps(nextProps) {
        clearInterval(this.timerID);
        this.init(nextProps.url);
    };

    componentWillUnmount() {
        clearInterval(this.timerID);
    };

    init = (url) => {
        this.fetch({
            url: url
        });

        this.timerID = setInterval(
            () => this.fetch({
                url: url
            }), Properties.getPiceHostInfoURL_TIME
        )
    };
    fetch = (params = {}) => {
        let that = this;
        reqwest({
            url: params.url
            , method: 'get'
            , type: 'json'
            , contentType: 'application/json'
            , success(resp) {
                console.log(resp);
                if (resp.success) {
                    that.setState({dataSource: resp.dataList})
                }
            }, error(resp) {
                console.log(resp)
            }
        });
    };

    render() {
        const columns = [
            {name: "ID", width: '30%'},
            {name: "型号", width: '40%'},
            {name: "状态"},
            {name: "大小"},
        ];
        // const dataSource = [
        //     {
        //         id: "aljdafdslkjfasdof",
        //         primary: "data01",
        //         status: "Healthy",
        //         size: "800G",
        //     }
        // ];
        return (
            <table className="pcie-table" cellSpacing={0} cellPadding={0}>
                <thead>
                <tr>
                    {
                        columns.map(function (item, index) {
                            return (
                                <th width={item.width} key={index}>{item.name}</th>
                            )
                        })
                    }
                </tr>
                </thead>
                <tbody>
                {
                    this.state.dataSource.map(function (item, index) {
                        return (
                            <tr key={index}>
                                <td>{item.id}</td>
                                <td>{item.model}</td>
                                <td>{item.status}</td>
                                <td>{item.size}</td>
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        )

    }
}