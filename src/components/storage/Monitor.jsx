import React from 'react';

import Title from '../common/Title';
import * as constants from '../constants/Constants';


import Properties from '../constants/Properties';
import HorizontalBar from '../calc/MountPointUseBar';
import RowOneAndTwo from '../calc/RowOneAndTwo';
import RowFour from '../calc/RowFour';
import PCIETable from '../storage/PCIETable';

export default class Monitor extends React.Component {
    render() {
        const deviceId = this.props.deviceId;
        const list1 = [
            {
                title: constants.MEMORY_STATUS,
                id: "MEMORY_STATUS",
                minHeight: "115px",
                url: Properties.getMemoryStatusURL + deviceId,
            },
            {
                title: constants.IO_STATUS,
                id: "IO_STATUS",
                minHeight: "115px",
                url: Properties.getIOLoadURL + deviceId,
            }
        ];
        const list3 = [
            {
                title: constants.PCIE_DATA,
                id: "PCIE_Contril_IO",
                minHeight: "115px",
                url: Properties.getPiceDataInfoURL + deviceId
            },
            {
                title: constants.PCIE_CONTROLLER,
                id: "PCIE_LATENCY",
                minHeight: "115px",
                url: Properties.getPiceControllerInfoURL + deviceId
            }
        ];

        return (
            <div className="Monitor">
                <div className="left">
                    <RowOneAndTwo list={list1}
                                  loadURL={Properties.getLoadAverageByDeviceIdURL + deviceId}
                                  configURL={Properties.getDeviceStatusURL + deviceId}
                                  cpuURL={Properties.getCpuStatusURL + deviceId}
                    />
                    <RowFour list={list3}
                             oneTitle={constants.PCIE_HOST}
                             oneURL={Properties.getPiceHostInfoURL + deviceId}
                             logURL={Properties.getMonitorLoginfoURL}
                             id={deviceId}
                    />
                </div>
                <div className="right">
                    <div className="item-list">
                        <Title title={constants.MountPointUse}/>
                        <HorizontalBar id="MountPointUse" url={Properties.getMountPointUseURL + deviceId} minHeight="220px"/>
                    </div>
                    <div className="item-list">
                        <Title title={constants.PCE_E_MSG}/>
                        <PCIETable url={Properties.getPcieInfoURL + deviceId}/>
                    </div>
                </div>
            </div>
        )
    }
}